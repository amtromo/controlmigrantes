from django import forms
from .models import Migrante
from django.core.exceptions import ValidationError
import datetime


class MigranteForm(forms.ModelForm):

    def clean(self):
        data = super().clean()
        print(data)
        id = data.get('identificacion')
        tipo_id = data.get('tipo_identificacion')
        fecha_nacimiento = data.get('fecha_nacimiento')
        fecha_ingreso = data.get('fecha_ingreso')

        if tipo_id == 'Ninguna' and id != 'Desconocida':
            raise ValidationError('Se trato de registrar una identificacion nula, con campos erroneos, ¡rechazado!')

        ahora = datetime.datetime.now()
        fecha_ahora = ahora.strftime("%Y-%m-%d")
        x = isinstance(fecha_nacimiento, str)
        if fecha_nacimiento is None:
            raise ValidationError('Hubo un problema con la fecha reingresala')

        elif x is False:
            fecha_nac = fecha_nacimiento.strftime("%Y-%m-%d")
            if fecha_ahora < fecha_nac:
                raise ValidationError('Fecha futura, imposible que sea de nacimiento')
        else:
            if fecha_ahora < fecha_nacimiento:
                raise ValidationError('Fecha futura, imposible que sea de nacimiento')

        x = isinstance(fecha_ingreso, str)
        if fecha_ingreso is None:
            raise ValidationError('Hubo un problema con la fecha reingresala')

        elif x is False:
            fecha_ing = fecha_ingreso.strftime("%Y-%m-%d")
            if fecha_ahora < fecha_ing:
                raise ValidationError('Fecha futura, imposible que sea de ingreso')
        else:
            if fecha_ahora < fecha_ingreso:
                raise ValidationError('Fecha futura, imposible que sea de ingreso')

        if tipo_id != 'Ninguna' and tipo_id != 'Visa' and tipo_id != 'Pasaporte' and tipo_id != 'Identificacion' and tipo_id != 'Licencia':
            raise ValidationError('El tipo de identificacion es invalido')

        sex = data.get('sexo')

        if sex != 'Indefinido' and sex != 'Hombre' and sex != 'Mujer':
            raise ValidationError('Sexo no registrado')

        if((data.get('imagen_migrante') is None) or (data.get('imagen_firma') is None)):
            if((data.get('imagen_migrante') is None)):
                raise ValidationError('No se cargo imagen de migrante, cargarla de favor')
            if((data.get('imagen_firma') is None)):
                raise ValidationError('No se cargo imagen de firma, cargarla de favor')
        else:
            file_size_mi = data.get('imagen_migrante').size
            file_size_fi = data.get('imagen_firma').size
            limit_mb = 8

            if file_size_mi > limit_mb * 1024 * 1024:
                raise ValidationError("Max size of file is %s MB" % limit_mb)

            if file_size_fi > limit_mb * 1024 * 1024:
                raise ValidationError("Max size of file is %s MB" % limit_mb)

    class Meta:

        model = Migrante
        fields = '__all__'
        widgets = {
            'nombre': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'materno': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'paterno': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'pais_origen': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'fecha_nacimiento': forms.DateInput({"class": "form-control", "oninput": "this.className", "readonly": "true"}),
            'fecha_ingreso': forms.DateInput({"class": "form-control", "oninput": "this.className", "readonly": "true"}),
            'sexo': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'tipo_identificacion': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'identificacion': forms.TextInput({"class": "form-control", "oninput": "this.className", "value": "Desconocida", "readonly": "true"}),
            'pais_destino': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'ciudad_destino': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'familiares_destino': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'familiares_mexico': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'motivo': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'intentos': forms.NumberInput({"class": "form-control", "oninput": "this.className", "min": "0"}),
            'lugar_ingreso': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'estado': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'hijos': forms.NumberInput({"class": "form-control", "oninput": "this.className", "min": "0", "max": "16"}),
            'direccion': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'telefono_contacto': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'telefono_recados': forms.TextInput({"class": "form-control", "oninput": "this.className"}),
            'padecimiento': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'padecimiento_descripcion': forms.Textarea({"class": "form-control", "oninput": "this.className", "readonly": "true"}),
            'accidentes': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'lugar_accidente': forms.Textarea({"class": "form-control", "oninput": "this.className", "readonly": "true"}),
            'atracos': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'lugar_atraco': forms.Textarea({"class": "form-control", "oninput": "this.className", "readonly": "true"}),
            'denuncio': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'con_albergue': forms.Select({"class": "form-control", "oninput": "this.className"}),
            'otros_al': forms.CheckboxInput({"class": "form-control", "oninput": "this.className"}),
            'albergues_descripcion': forms.Textarea({"class": "form-control", "oninput": "this.className", "readonly": "true"}),
            'asistencias': forms.SelectMultiple({"class": "form-control", "oninput": "this.className"}),
            'observaciones': forms.Textarea({"class": "form-control", "oninput": "this.className"}),
            'imagen_migrante': forms.FileInput({"class": "form-control upload_btn", "oninput": "this.className"}),
            'imagen_firma': forms.FileInput({"class": "form-control upload_btn", "oninput": "this.className"}),
            'aceptado': forms.CheckboxInput({'style': 'display:none;'}),
            'activo': forms.CheckboxInput({'style': 'display:none;'}),
            'eliminado': forms.CheckboxInput({'style': 'display:none;'})}
