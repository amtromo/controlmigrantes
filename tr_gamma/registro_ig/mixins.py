#  Created by Manuel Garcia
#  Description : Mixins file for ajax updating
#  Modified by: Manuel Garcia
#  Modify date: 19/10/2018

from django.http import JsonResponse


class AjaxFormMixin(object):
    def form_invalid(self, form):
        response = super(AjaxFormMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def form_valid(self, form):
        response = super(AjaxFormMixin, self).form_valid(form)
        if self.request.is_ajax():
            print(form.cleaned_data)
            data = {
                'message': "Se ha actualizado la informacion."
            }
            return JsonResponse(data)
        else:
            return response
