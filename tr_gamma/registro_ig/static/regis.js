// # Created by Manuel Garcia
// # Description : Javascript for registration
// # Modified by: Manuel Garcia
// # Modify date: 15/10/2018

var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";

  // ... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Registrar";
  } else {
    document.getElementById("nextBtn").innerHTML = "Siguiente";
  }
  // ... and run a function that displays the correct step indicator:
  fixStepIndicator(n)

  x[0].style.display = "block";
  x[1].style.display = "block";
  x[2].style.display = "block";
  x[3].style.display = "block";
  x[4].style.display = "block";
  x[5].style.display = "block";
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form... :
  if (currentTab >= x.length) {
    //...the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, z, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");

  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  z = x[currentTab].getElementsByTagName("select");

  // A loop that checks every input field in the current tab:
  for (i = 0; i < z.length; i++) {
    // If a field is empty...
    if (z[i].value == "") {
      // add an "invalid" class to the field:
      z[i].className += " is-invalid";
      // and set the current valid status to false:
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class to the current step:
  x[n].className += " active";
}
if($("#notif_error").length){

  document.getElementById("notif_error").onclick = close;

}
//


function close() {
  $('#exampleModalCenter').modal('hide');
  $('#exampleModalCenter').html("");
}

$(function () {
       $('.datetime-input').datetimepicker({
           format:'YYYY-MM-DD',

       });
   });
$(window).on('load',function(){
        $('#exampleModalCenter').modal('show');
        $("#regForm select").each(function(index){
          if ($(this).has('option:selected')){
        		if($(this).val()=="" || $(this).val()=="---------"){
                $(this).addClass("is-invalid");

            }
          }
});

  });

  $('select').each(function(){
      $(this).change(function(){
        if ($(this).hasClass('is-invalid')){
        $(this).removeClass("is-invalid");

      }
      });
  });

  $('id_fecha_nacimiento').change(function(){
    if ($('id_fecha_nacimiento').hasClass('invalid')){
      $('id_fecha_nacimiento').removeClass("invalid");

  }
  });
  $('id_fecha_ingreso').change(function(){
    if ($('id_fecha_ingreso').hasClass('invalid')){
      $('id_fecha_ingreso').removeClass("invalid");

  }
  });

  $('input:checkbox').each(function(){
      $(this).change(function(){
        if ($(this).prop("checked")){
          switch ($(this).attr('id')) {
            case 'id_padecimiento':
                $('#id_padecimiento_descripcion').prop('readonly',false);
              break;
              case 'id_accidentes':
                  $('#id_lugar_accidente').prop('readonly',false);
                break;
                case 'id_atracos':
                    $('#id_lugar_atraco').prop('readonly',false);
                  break;
                  case 'id_otros_al':
                      $('#id_albergues_descripcion').prop('readonly',false);
                    break;

            default:

          }

        }
        else{
          switch ($(this).attr('id')) {
            case 'id_padecimiento':
                $('#id_padecimiento_descripcion').prop('readonly',true);
              break;
              case 'id_accidentes':
                  $('#id_lugar_accidente').prop('readonly',true);
                break;
                case 'id_atracos':
                    $('#id_lugar_atraco').prop('readonly',true);
                  break;
                  case 'id_otros_al':
                      $('#id_albergues_descripcion').prop('readonly',true);
                    break;

            default:

          }
        }
      });
  });



$('#id_tipo_identificacion').on('change', function() {
    if ($('#id_tipo_identificacion').val() == "Ninguna"){
        $("#id_identificacion").prop('readonly', true);
        $("#id_identificacion").val('Desconocida');
        $("#nextBtn").prop('disabled', false);
    }else{
        $("#id_identificacion").prop('readonly', false);
        $("#id_identificacion").val('');
    }
  });

  $("#id_identificacion").on('input',function () {
        var identificacion = $(this).val();
       $.ajax({
          url: '/id_check/',
          data: {
            'id_identificacion': identificacion
          },
          dataType: 'json',
          type: 'POST',
          success: function (data) {
            console.log(data)
            if (!data.is_valid) {
              $("#nextBtn").prop('disabled', true);
              alert("La identificacion ya ha sido registrada");

            }else{
              $("#nextBtn").prop('disabled', false);
            }
          }
        });
     });






     var today, datepicker, datepicker2;
     today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
     datepicker = $('#id_fecha_nacimiento').datepicker({
         maxDate: today,
         format: 'dd/mm/yyyy',
         minDate: '1900-01-01',
         uiLibrary: 'bootstrap4'
     });
     datepicker2 = $('#id_fecha_ingreso').datepicker({
       maxDate: today,
       format: 'dd/mm/yyyy',
       minDate: '1900-01-01',
       uiLibrary: 'bootstrap4'
   });


$('#id_imagen_firma').on('change', function() {
      if ($('#id_imagen_firma').is(":invalid")) {
          var file_firma = $('#id_imagen_firma')[0].files[0];
          if (file_firma){
            $('#btn_id_imagen_firma').text(file_firma.name);
            $('#id_imagen_firma').removeClass('invalid');
            $('#btn_id_imagen_firma').removeClass('btn-outline-danger');
            $('#btn_id_imagen_firma').addClass('btn-outline-success');
          }
        } else{
          var file_firma = $('#id_imagen_firma')[0].files[0];
          if (file_firma){
            $('#btn_id_imagen_firma').text(file_firma.name);
            $('#btn_id_imagen_firma').removeClass('btn-outline-danger');
            $('#btn_id_imagen_firma').addClass('btn-outline-success');
          }
    }
  });

  $('#id_imagen_migrante').on('change', function() {
        if ($('#id_imagen_migrante').is(":invalid")) {
            var file_firma = $('#id_imagen_migrante')[0].files[0];
            if (file_firma){
              $('#btn_id_imagen_migrante').text(file_firma.name);
              $('#id_imagen_migrante').removeClass('invalid');
              $('#btn_id_imagen_migrante').removeClass('btn-outline-danger');
              $('#btnid_imagen_migrante').addClass('btn-outline-success');
            }
          } else{
            var file_firma = $('#id_imagen_migrante')[0].files[0];
            if (file_firma){
              $('#btn_id_imagen_migrante').text(file_firma.name);
              $('#btn_id_imagen_migrante').removeClass('btn-outline-danger');
              $('#btn_id_imagen_migrante').addClass('btn-outline-success');
            }
      }
    });


// Ajax registration

// Json Request to add new Motivo de Migracion
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

ajax_modal_title = document.getElementById('AjaxRegistrationTitle');
ajax_new_entry_label = document.getElementById('new_entry_label');
ajax_add_btn = document.getElementById('addAjax');


$('#ajax_btn_11').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Motivo de Migracion";
      ajax_new_entry_label.innerHTML = "Motivo: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxMot";

      $("#addAjaxMot").unbind( "click" ).bind('click', function(){
        var motivo = $('#new_entry').val();
        $.ajax({
           url: '/add_motivo/',
           data: {
             'motivo': motivo
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_motivo').html(data.select);
               $('#id_motivo').val(data.option);
               $('#id_motivo').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});


// Json Request to add new Lugar de Ingreso
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

$('#ajax_btn_15').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Lugar de Ingreso";
      ajax_new_entry_label.innerHTML = "Lugar: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxLug";

      $("#addAjaxLug").unbind( "click" ).bind('click', function(){
        var lugar_ingreso = $('#new_entry').val();
        $.ajax({
           url: '/add_lugar_ingreso/',
           data: {
             'lugar_ingreso': lugar_ingreso
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_lugar_ingreso').html(data.select);
               $('#id_lugar_ingreso').val(data.option);
               $('#id_lugar_ingreso').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});

// Json Request to add new Lugar de Ingreso
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

$('#ajax_btn_28').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Razon de Conocimiento";
      ajax_new_entry_label.innerHTML = "Razon: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxCon";

      $("#addAjaxCon").unbind( "click" ).bind('click', function(){
        var con_albergue = $('#new_entry').val();
        $.ajax({
           url: '/add_con_albergue/',
           data: {
             'con_albergue': con_albergue
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_con_albergue').html(data.select);
               $('#id_con_albergue').val(data.option);
               $('#id_con_albergue').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});


// Json Request to add new Tipo de Asistencia
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

$('#ajax_btn_31').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Tipo de Asistencia";
      ajax_new_entry_label.innerHTML = "Tipo: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxTipo";

      $("#addAjaxTipo").unbind( "click" ).bind('click', function(){
        var tipo_asistencia = $('#new_entry').val();
        $.ajax({
           url: '/add_asistencias/',
           data: {
             'tipo_asistencia': tipo_asistencia
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_asistencias').html(data.select);
               $('#id_asistencias').val(data.option);
               $('#id_asistencias').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});
