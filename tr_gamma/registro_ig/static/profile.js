// Created by Manuel Garcia
// Description : Registration of a migrant template with form
// Modified by: Manuel Garcia
// Modify date: 19/10/2018

var aux = "";

// Json Request to get Observaciones
// Parameters: Id of migrant
// Return: Text Field of obervaciones and the content of it
var text = '<h3 id="obs_title">Observaciones</h3><div class="container" id="obs_container"><div class="row"><div class="col"><textarea name="observaciones" cols="40" rows="10" class="form-control" id="id_obs" placeholder="No se han realizado observaciones" readonly=true></textarea></div></div></div><div class="container" id="btn_container"><div class="row" id="buttons_row"><div class="col"><button type="button" class="btn btn-primary float-right" id="edit_obs" onclick="change_btn()" name="button">Editar</button></div></div>';

$('#ajax_observaciones').unbind("click").bind('click', function() {
  $('#ajax_observaciones').addClass('active');
  $('#ajaxProfileInfo').removeClass('active');
  $('#ajax_space').removeClass('scrollable-content');
  $.ajax({
    url: '/get_observaciones/',
    data: {
      'id': id
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {
        $('#ajax_space').html(text);
        if (data.obs != "") {
          $('#id_obs').val(data.obs);
          aux = $('#id_obs').val();
        }
        $("#id_obs").prop("readonly", true);

      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });
});

// Change buttons availables
// Parameters: None
// Return: Edit and Cancel buttons with the removal of readonly
function change_btn() {
  $("#btn_container").html('<div class="row" id="buttons_row_edit"><div class="col-auto mr-auto"><button type="button" class="btn btn-danger" id="cancel_obs" name="button" onclick="revert_btn()">Cancelar</button></div><div class="col-auto"><button type="button" class="btn btn-success" id="save_obs" name="button" onclick="save_obs()">Guardar</button></div></div>');
  $("#id_obs").prop("readonly", false);
}

// Get edit button and eliminate others
// Parameters: None
// Return: Edit button and lock
function revert_btn() {
  $("#btn_container").html('<div class="row" id="buttons_row"><div class="col"><button type="button" class="btn btn-primary float-right" id="edit_obs" name="button" onclick="change_btn()">Editar</button></div>');
  $("#id_obs").val(aux);
  $("#id_obs").prop("readonly", true);
}

// Load modal, bind operations and makea vailable ajax of updating
// Parameters: None
// Return: Edited field of Observaciones
function save_obs() {
  $('#modal_confirmation').html('<div class="modal fade" id="modal_con" tabindex="-1" role="dialog" aria-labelledby="modal_con" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalCenterTitle">Confirmar cambio en observaciones</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><p>¿Estas seguro de que deseas actualizar las observaciones ?</p></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal" id="cancel_mod_con">Cancelar</button><button type="button" class="btn btn-primary" id="updateAjaxObs">Guardar</button></div></div></div></div>');
  $('#cancel_mod_con').click(function() {
    $('#modal_con').modal('hide');
    revert_btn();
  });

  $("#updateAjaxObs").unbind("click").bind('click', function() {
    var obs = $('#id_obs').val();
    if (obs != aux) {
      $.ajax({
        url: '/update_obs/',
        data: {
          'id': id,
          'obs': obs
        },
        dataType: 'json',
        type: 'POST',
        success: function(data) {
          console.log(data)
          if (data.is_valid) {
            $('#ajax_observaciones').click();
            retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> ' + data.message + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
            $('#retro_ajax').html(retro);
            setTimeout(function() {
              $("#retro_alert_good").alert('close');
              $(".good").alert('close');
            }, 5000);

          } else {
            retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
            $('#retro_ajax').html(retro);
            setTimeout(function() {
              $("#retro_alert_bad").alert('close');
              $(".bad").alert('close');
            }, 5000);
          }
        }
      });
    } else {
      retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Ningun cambio realizado:</strong> no haz editado la informacion' + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
      $('#retro_ajax').html(retro);
      setTimeout(function() {
        $("#retro_alert_bad").alert('close');
        $(".bad").alert('close');
      }, 5000);
    }
    $('#modal_con').modal('hide');

  });
  $('#modal_con').modal('show');
}





// Load modal, bind operations and makea vailable ajax of updating via getting activo field from migrants
// Parameters: None
// Return: Edited field of activo


function load_btn_activo() {
  get_field_load('activo', function(data) {
    if (data.field)
      $('#div_btn_activo').html('<button type="button" id="btn_activ" class="btn btn-success" data-dismiss="modal" aria-label="Close" >Activo</button>');
    else {
      $('#div_btn_activo').html('<button type="button" id="btn_activ" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Fuera del Albergue</button>');
    }

    $('#btn_activ').bind('click', function() {
      if (data.field) {
        $('#modal_confirmation').html('<div class="modal fade" id="modal_act" tabindex="-1" role="dialog" aria-labelledby="modal_act" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalCenterTitle">Cambiar estado de migrante en el albergue</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><p>¿Estas seguro de cambiar el estado del migrante de activo a <strong> fuera del albergue </strong>?</p></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal" id="cancel_mod_act">Cancelar</button><button type="button" class="btn btn-primary" id="updateAjaxAct">Cambiar</button></div></div></div></div>');
        $('#cancel_mod_act').click(function() {
          $('#modal_act').modal('hide');
        });
      } else {
        $('#modal_confirmation').html('<div class="modal fade" id="modal_act" tabindex="-1" role="dialog" aria-labelledby="modal_act" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalCenterTitle">Cambiar estado de migrante en el albergue</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><p>¿Estas seguro de cambiar el estado del migrante de fuera del albergue a <strong> activo </strong>?</p></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal" id="cancel_mod_act">Cancelar</button><button type="button" class="btn btn-primary" id="updateAjaxAct">Cambiar</button></div></div></div></div>');
        $('#cancel_mod_act').click(function() {
          $('#modal_act').modal('hide');
        });
      }
      $("#updateAjaxAct").unbind("click").bind('click', function() {
        $.ajax({
          url: '/update_act/',
          data: {
            'id': id
          },
          dataType: 'json',
          type: 'POST',
          success: function(data) {
            console.log(data)
            if (data.is_valid) {
              retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> ' + data.message + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
              $('#retro_ajax').html(retro);
              load_btn_activo();
              if (document.getElementById('id_obs')!= null){
                $('#ajax_observaciones').click();
              }
              load_fecha_salida();
              setTimeout(function() {
                $("#retro_alert_good").alert('close');
                $(".good").alert('close');
              }, 5000);

            } else {
              retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
              $('#retro_ajax').html(retro);
              load_btn_activo();
              setTimeout(function() {
                $("#retro_alert_bad").alert('close');
                $(".bad").alert('close');
              }, 5000);
            }
          }
        });

        $('#modal_act').modal('hide');

      });
      $('#modal_act').modal('show');
    });


  }, function(data) {
    retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
    $('#retro_ajax').html(retro);
    setTimeout(function() {
      $("#retro_alert_bad").alert('close');
      $(".bad").alert('close');
    }, 5000);
  });
}



// Load modal, bind operations and makea vailable ajax of updating via getting aceptado field from migrants
// Parameters: None
// Return: Edited field of aceptado


function load_btn_aceptado() {
  get_field_load('aceptado', function(data) {
    if (data.field)
      $('#div_btn_aceptado').html('<button type="button" id="btn_acept" class="btn btn-outline-success" data-dismiss="modal" aria-label="Close">Aceptado</button>');
    else {
      $('#div_btn_aceptado').html('<button type="button" id="btn_acept" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Vetado</button>');
    }

    $('#btn_acept').bind('click', function() {
      if (data.field) {
        $('#modal_confirmation').html('<div class="modal fade" id="modal_acept" tabindex="-1" role="dialog" aria-labelledby="modal_acept" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalCenterTitle"><strong>Vetar al migrante</strong></h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><p>¿Estas seguro de querer <strong> vetar al migrante </strong> del albergue?</p></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel_mod_acept">Cancelar</button><button type="button" class="btn btn-danger" id="updateAjaxAcept">Vetar</button></div></div></div></div>');
        $('#cancel_mod_acept').click(function() {
          $('#modal_acept').modal('hide');
        });
      } else {
        $('#modal_confirmation').html('<div class="modal fade" id="modal_acept" tabindex="-1" role="dialog" aria-labelledby="modal_acept" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalCenterTitle">Aceptar de nuevo al migrante</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><p>¿Deseas <strong>aceptar de nuevo al migrante</strong> en el albergue?</p></div><div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal" id="cancel_mod_acept">Cancelar</button><button type="button" class="btn btn-primary" id="updateAjaxAcept">Aceptar</button></div></div></div></div>');
        $('#cancel_mod_acept').click(function() {
          $('#modal_acept').modal('hide');
        });
      }
      $("#updateAjaxAcept").unbind("click").bind('click', function() {
        $.ajax({
          url: '/update_acept/',
          data: {
            'id': id
          },
          dataType: 'json',
          type: 'POST',
          success: function(data) {
            console.log(data)
            if (data.is_valid) {
              retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> ' + data.message + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
              $('#retro_ajax').html(retro);
              load_btn_aceptado();
              if (document.getElementById('id_obs')!= null){
                $('#ajax_observaciones').click();
              }
              setTimeout(function() {
                $("#retro_alert_good").alert('close');
                $(".good").alert('close');
              }, 5000);

            } else {
              retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
              $('#retro_ajax').html(retro);
              load_btn_aceptado();
              setTimeout(function() {
                $("#retro_alert_bad").alert('close');
                $(".bad").alert('close');
              }, 5000);
            }
          }
        });

        $('#modal_acept').modal('hide');

      });
      $('#modal_acept').modal('show');
    });


  }, function(data) {
    retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
    $('#retro_ajax').html(retro);
    setTimeout(function() {
      $("#retro_alert_bad").alert('close');
      $(".bad").alert('close');
    }, 5000);
  });
}

// Get fecha salida if not none then print on template
// Parameters: None
// Return: row of fecha salida

function load_fecha_salida(){
  get_field_load('fecha_ultima_salida',function(data){
    if(data.field != null){
      date = new Date(data.field);
      $('#li_fecha_sal').html('<li class="list-group-item"><span class="pull-left"><strong>Fecha ultima salida</strong></span> '+ date +' </li>');

    }
  },function(data){
    retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
    $('#retro_ajax').html(retro);
    setTimeout(function() {
      $("#retro_alert_bad").alert('close');
      $(".bad").alert('close');
    }, 5000);
  });
}

// Get field and exec functionions if true or false
// Parameters: None
// Return: Edited field of Observaciones

function get_field_load(field, when_true, when_false) {
  $.ajax({
    url: '/get_field/',
    data: {
      'id': id,
      'field': field
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {
        when_true(data);

      } else {
        when_false(data);
      }
    }

  });
}

// load url for imagen migrante
// Parameters: None
// Return: Img url

function load_imagen_p_url(){
  get_field_load('imagen_migrante', function(data){
    $('#img_mig_p').prop("src",'/media/'+data.field);
    console.log(data.field.url);
  }, function(data){
    cretro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
    $('#retro_ajax').html(retro);
    setTimeout(function() {
      $("#retro_alert_bad").alert('close');
      $(".bad").alert('close');
    }, 5000);
  });

}

// load url for imagen firma
// Parameters: None
// Return: Img url
function load_imagen_f_url(){
  get_field_load('imagen_firma', function(data){
    $('#firma_imagen').prop("src",'/media/'+data.field);
    console.log(data.field.url);
  }, function(data){
    retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
    $('#retro_ajax').html(retro);
    setTimeout(function() {
      $("#retro_alert_bad").alert('close');
      $(".bad").alert('close');
    }, 5000);
  });
}


$('#edit_prof').bind('click',function(){

  $('#img_show').prop('src', imagen_p)
    $('#firmaModal').modal('hide');
  $('#imgModal').modal('show');
  up_img('perfil');
});

$('#img_firm_mod').bind('click',function(){
  $('#img_show').prop('src', imagen_f)
  $('#imgModal').modal('show');
  up_img('firma');
});



// Upload image with ajax
// Parameters: None
// Return: Img uploaded and retro

function up_img(type_str){
  $("#ajax_img").unbind("click").bind('click', function() { 
   formdata = new FormData();    
    var file = $('#logo-id')[0].files[0];
    if ($('#logo-id').val()) {
      formdata.append("img", file);
      formdata.append("type_str", type_str);
      formdata.append("id", id);
      $.ajax({
        type: 'POST',
        url: '/update_img/',
        data: formdata,
        processData: false,
        contentType: false,

        success: function(data) {
          console.log(data)
          if (data.is_valid) {
            retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> ' + data.message + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
            $('#retro_ajax').html(retro);
            if(type_str=='firma'){
              load_imagen_f_url();
            }else{
              load_imagen_p_url();
            }

            setTimeout(function() {
              $("#retro_alert_good").alert('close');
              $(".good").alert('close');
            }, 5000);

          } else {
            retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
            $('#retro_ajax').html(retro);
            setTimeout(function() {
              $("#retro_alert_bad").alert('close');
              $(".bad").alert('close');
            }, 5000);
          }
        }
      });
    } else {
      retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Ningun cambio realizado:</strong> no haz agregado una imagen' + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
      $('#retro_ajax').html(retro);
      setTimeout(function() {
        $("#retro_alert_bad").alert('close');
        $(".bad").alert('close');
      }, 5000);
    }
    $('#imgModal').modal('hide');

  });
}





// Show preview
// Parameters: None
// Return: preview

$(document).ready(function() {
    var brand = document.getElementById('logo-id');
    brand.className = 'attachment_upload';
    brand.onchange = function() {
        document.getElementById('fakeUploadLogo').value = this.value.substring(12);
    };

    // Source: http://stackoverflow.com/a/4459419/6396981
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#logo-id").change(function() {
        readURL(this);
    });
});



// Load modal, bind operations and makea vailable ajax of updating
// Parameters: None
// Return: Edited field of Observaciones

  $("#eliminar_migrante").unbind("click").bind('click', function() {
    var string = $('#borrar_confirmacion').val();
    console.log(string);
      $.ajax({
        url: '/delete_migrant/',
        data: {
          'id': id,
          'string': string
        },
        dataType: 'json',
        type: 'POST',
        success: function(data) {
          console.log(data)
          if (data.is_valid) {
            window.location.replace("/");
          } else {
            retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
            $('#retro_ajax').html(retro);
            setTimeout(function() {
              $("#retro_alert_bad").alert('close');
              $(".bad").alert('close');
            }, 5000);
            $('#adm_modal').modal('hide');
          }
        }
      });
  });



// Load buttons when loading page
// Parameters: None
// Return: Buttons loaded

$(window).on('load', function() {
  load_btn_activo();
  load_btn_aceptado();
  load_fecha_salida();
  load_imagen_p_url();
  setTimeout(function() {
    $("#retro_alert_good").alert('close');
    $(".good").alert('close');
  }, 5000);
});
