// Created by Manuel Garcia
// Description :Home js
// Modified by: Manuel Garcia
// Modify date: 26/10/2018
$(document).ready(function() {
    $('#consulta').DataTable( {
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "ajax": "/get_consultas/",
        "columns": [
            { "data": "activo" },
            { "data": "sistema" },
            { "data": "nombre",
                  "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                  $(nTd).html("<a href='/migrante/"+oData.sistema+"'>"+oData.nombre+"</a>");
              }
            },
            { "data": "fecha_ingreso" },
            { "data": "identificacion" },
            { "data": "origen" },
            { "data": "destino" },
            { "data": "fecha_nacimiento" },
            { "data": "direccion" },
            { "data": "contacto" },
            { "data": "fecha_registro" },
            { "data": "ultima_salida" }
        ]
    } );
} );
