// # Created by Manuel Garcia
// # Description : Javascript for edit
// # Modified by: Manuel Garcia
// # Modify date: 19/10/2018

$('#notif_error').bind('click',close);
function close() {
  $('#exampleModalCenter').modal('hide');
  $('#exampleModalCenter').html("");
}

$(function () {
       $('.datetime-input').datetimepicker({
           format:'YYYY-MM-DD',

       });
   });
$(window).on('load',function(){
        $('#exampleModalCenter').modal('show');
        $("#regForm select").each(function(index){
          if ($(this).has('option:selected')){
        		if($(this).val()=="" || $(this).val()=="---------"){
                $(this).addClass("is-invalid");

            }
          }
});

  });

  $('select').each(function(){
      $(this).change(function(){
        if ($(this).hasClass('is-invalid')){
        $(this).removeClass("is-invalid");

      }
      });
  });

  $('id_fecha_nacimiento').change(function(){
    if ($('id_fecha_nacimiento').hasClass('invalid')){
      $('id_fecha_nacimiento').removeClass("invalid");

  }
  });

  $('id_fecha_ingreso').change(function(){
    if ($('id_fecha_ingreso').hasClass('invalid')){
      $('id_fecha_ingreso').removeClass("invalid");

  }
  });

  $('input:checkbox').each(function(){
      $(this).change(function(){
        if ($(this).prop("checked")){
          switch ($(this).attr('id')) {
            case 'id_padecimiento':
                $('#id_padecimiento_descripcion').prop('readonly',false);
              break;
              case 'id_accidentes':
                  $('#id_lugar_accidente').prop('readonly',false);
                break;
                case 'id_atracos':
                    $('#id_lugar_atraco').prop('readonly',false);
                  break;
                  case 'id_otros_al':
                      $('#id_albergues_descripcion').prop('readonly',false);
                    break;

            default:

          }

        }
        else{
          switch ($(this).attr('id')) {
            case 'id_padecimiento':
                $('#id_padecimiento_descripcion').prop('readonly',true);
              break;
              case 'id_accidentes':
                  $('#id_lugar_accidente').prop('readonly',true);
                break;
                case 'id_atracos':
                    $('#id_lugar_atraco').prop('readonly',true);
                  break;
                  case 'id_otros_al':
                      $('#id_albergues_descripcion').prop('readonly',true);
                    break;

            default:

          }
        }
      });
  });



$('#id_tipo_identificacion').on('change', function() {
    if ($('#id_tipo_identificacion').val() == "Ninguna"){
        $("#id_identificacion").prop('readonly', true);
        $("#id_identificacion").val('Desconocida');
        $("#nextBtn").prop('disabled', false);
    }else{
        $("#id_identificacion").prop('readonly', false);
        $("#id_identificacion").val('');
    }
  });

  $("#id_identificacion").on('input',function () {
        var identificacion = $(this).val();
       $.ajax({
          url: '/id_check/',
          data: {
            'id_identificacion': identificacion,
            'inst_id': inst_id
          },
          dataType: 'json',
          type: 'POST',
          success: function (data) {
            console.log(data)
            if (!data.is_valid) {
              $("#nextBtn").prop('disabled', true);
              alert("La identificacion ya ha sido registrada");

            }else{
              $("#nextBtn").prop('disabled', false);
            }
          }
        });
     });






var today, datepicker, datepicker2;
    today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
    datepicker = $('#id_fecha_nacimiento').datepicker({
        maxDate: today,
        format: 'dd/mm/yyyy',
        minDate: '1900-01-01',
        uiLibrary: 'bootstrap4'
    });
    datepicker2 = $('#id_fecha_ingreso').datepicker({
      maxDate: today,
      format: 'dd/mm/yyyy',
      minDate: '1900-01-01',
      uiLibrary: 'bootstrap4'
  });


$('#id_imagen_firma').on('change', function() {
      if ($('#id_imagen_firma').is(":invalid")) {
          var file_firma = $('#id_imagen_firma')[0].files[0];
          if (file_firma){
            $('#btn_id_imagen_firma').text(file_firma.name);
            $('#id_imagen_firma').removeClass('invalid');
            $('#btn_id_imagen_firma').removeClass('btn-outline-danger');
            $('#btn_id_imagen_firma').addClass('btn-outline-success');
          }
        } else{
          var file_firma = $('#id_imagen_firma')[0].files[0];
          if (file_firma){
            $('#btn_id_imagen_firma').text(file_firma.name);
            $('#btn_id_imagen_firma').removeClass('btn-outline-danger');
            $('#btn_id_imagen_firma').addClass('btn-outline-success');
          }
    }
  });

  $('#id_imagen_migrante').on('change', function() {
        if ($('#id_imagen_migrante').is(":invalid")) {
            var file_firma = $('#id_imagen_migrante')[0].files[0];
            if (file_firma){
              $('#btn_id_imagen_migrante').text(file_firma.name);
              $('#id_imagen_migrante').removeClass('invalid');
              $('#btn_id_imagen_migrante').removeClass('btn-outline-danger');
              $('#btnid_imagen_migrante').addClass('btn-outline-success');
            }
          } else{
            var file_firma = $('#id_imagen_migrante')[0].files[0];
            if (file_firma){
              $('#btn_id_imagen_migrante').text(file_firma.name);
              $('#btn_id_imagen_migrante').removeClass('btn-outline-danger');
              $('#btn_id_imagen_migrante').addClass('btn-outline-success');
            }
      }
    });


// Ajax registration

// Json Request to add new Motivo de Migracion
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

ajax_modal_title = document.getElementById('AjaxRegistrationTitle');
ajax_new_entry_label = document.getElementById('new_entry_label');
ajax_add_btn = document.getElementById('addAjax');


$('#ajax_btn_11').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Motivo de Migracion";
      ajax_new_entry_label.innerHTML = "Motivo: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxMot";

      $("#addAjaxMot").unbind( "click" ).bind('click', function(){
        var motivo = $('#new_entry').val();
        $.ajax({
           url: '/add_motivo/',
           data: {
             'motivo': motivo
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_motivo').html(data.select);
               $('#id_motivo').val(data.option);
               $('#id_motivo').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});


// Json Request to add new Lugar de Ingreso
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

$('#ajax_btn_15').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Lugar de Ingreso";
      ajax_new_entry_label.innerHTML = "Lugar: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxLug";

      $("#addAjaxLug").unbind( "click" ).bind('click', function(){
        var lugar_ingreso = $('#new_entry').val();
        $.ajax({
           url: '/add_lugar_ingreso/',
           data: {
             'lugar_ingreso': lugar_ingreso
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_lugar_ingreso').html(data.select);
               $('#id_lugar_ingreso').val(data.option);
               $('#id_lugar_ingreso').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});

// Json Request to add new Lugar de Ingreso
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

$('#ajax_btn_28').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Razon de Conocimiento";
      ajax_new_entry_label.innerHTML = "Razon: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxCon";

      $("#addAjaxCon").unbind( "click" ).bind('click', function(){
        var con_albergue = $('#new_entry').val();
        $.ajax({
           url: '/add_con_albergue/',
           data: {
             'con_albergue': con_albergue
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_con_albergue').html(data.select);
               $('#id_con_albergue').val(data.option);
               $('#id_con_albergue').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});


// Json Request to add new Tipo de Asistencia
// Parameters: Input of modal AjaxRegistration
// Return: Retroalimentation as alert and the refreshed select, selecting the option added

$('#ajax_btn_31').on('click', function(){
      ajax_modal_title.innerHTML = "Registrar Tipo de Asistencia";
      ajax_new_entry_label.innerHTML = "Tipo: ";
      $('#new_entry').val('')
      $('#AjaxRegistration').modal('show');
      ajax_add_btn.id = "addAjaxTipo";

      $("#addAjaxTipo").unbind( "click" ).bind('click', function(){
        var tipo_asistencia = $('#new_entry').val();
        $.ajax({
           url: '/add_asistencias/',
           data: {
             'tipo_asistencia': tipo_asistencia
           },
           dataType: 'json',
           type: 'POST',
           success: function (data) {
             console.log(data)
             if (data.is_valid) {
               $('#id_asistencias').html(data.select);
               $('#id_asistencias').val(data.option);
               $('#id_asistencias').removeClass('is-invalid');
               $('#AjaxRegistration').modal('hide');
               retro = '<div id="retro_alert_good" class="alert good alert-success alert-dismissible fade show" role="alert"><strong>Logrado</strong> '+data.message+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               $('#retro_ajax').html(retro);
               setTimeout(function(){$("#retro_alert_good").alert('close');$(".good").alert('close');}, 5000);

             }else{
               retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
               old_html = $('#modal_body_ajax').html();
               $('#modal_body_ajax').html(retro + old_html);
               setTimeout(function(){$("#retro_alert_bad").alert('close');$(".bad").alert('close');}, 5000);
             }
           }
         });
      });

});



$(document).ready(function(){
    var $myForm = $('#regForm')
    $myForm.submit(function(event){
        event.preventDefault()
        var $formData = $(this).serialize()
        var $thisURL = $myForm.attr('data-url') // or set your own url
        $.ajax({
            method: "POST",
            url: $thisURL,
            data: $formData,
            success: handleFormSuccess,
            error: handleFormError,
        })
    })

    function handleFormSuccess(data, textStatus, jqXHR){
        console.log(data)
        console.log(textStatus)
        console.log(jqXHR)
        $('#form_container').html('<div class="jumbotron"><h1 class="display-4">¡Exito se ha actualizado la información!</h1><p class="lead">La información ha sido actualiza en base a los parametros que cambiaste.</p><hr class="my-4"><p>Presiona el boton de aceptar para ir al perfil actualizado o atrás para volver a editar, ¡gracias!.</p><div class="container"><div class="row"><div class="col-auto mr-auto"><a class="btn btn-secondary btn-lg" href='+ url_editar +' role="button">Atrás</a></div><div class="col-auto"><a class="btn btn-primary btn-lg" href='+ url +' role="button">Aceptar</a></div></div></div></div');
    }

    function handleFormError(jqXHR, textStatus, errorThrown){
        console.log(jqXHR)
        console.log(textStatus)
        console.log(errorThrown)
    }
});

$('#confirm_btn').click(function(){
    $('#confirm').modal('hide');
    $("#submit_act").click();
});
