# Created by framework
# Description : Tests file for proving functionality in the application
# Modified by: Manuel Garcia
# Modify date: 15/10/2018

from django.test import RequestFactory, TestCase
from django.urls import resolve, reverse
from .models import Migrante, ConocerAlbergue, AsistenciaDada, LugarIngresoPais, MotivoMigracion
from django.core.files.uploadedfile import SimpleUploadedFile
import datetime
from django.conf import settings
import tempfile
from django.test import Client
from django.contrib.auth.models import AnonymousUser, User


# Create your tests here.
class MigranteTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='admin', email='admin@es.com', password='1234567890')
        settings.MEDIA_ROOT = tempfile.mkdtemp()
        self.conoce = ConocerAlbergue.objects.create(razon='Internet')
        self.asisten = AsistenciaDada.objects.create(tipo_asistencia='Primeros Auxilios')
        self.lug = LugarIngresoPais.objects.create(lugar='Guatemala')
        self.mot = MotivoMigracion.objects.create(motivo='Violencia')
        self.migrante = Migrante.objects.create(
            nombre='Ricardo',
            paterno='Contreras',
            materno='Quiroz',
            pais_origen='GB',
            fecha_nacimiento='1980-08-12',
            fecha_ingreso='1980-08-12',
            sexo='Hombre',
            tipo_identificacion='Ninguna',
            identificacion='Desconocida',
            pais_destino='AU',
            ciudad_destino='Chihuahua',
            motivo=self.mot,
            familiares_destino=True,
            intentos=1,
            familiares_mexico=False,
            lugar_ingreso=self.lug,
            estado='Casado',
            hijos=1,
            direccion='Palomares 24',
            telefono_contacto='4271098745',
            telefono_recados='4423450918',
            padecimiento=False,
            padecimiento_descripcion='',
            accidentes=True,
            lugar_accidente='Queretaro',
            atracos=False,
            lugar_atraco='',
            denuncio=False,
            con_albergue=self.conoce,
            otros_al=True,
            albergues_descripcion='Chiapas Refugio',
            observaciones='Llego en muy buen estado fisico',
            imagen_migrante=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png'),
            imagen_firma=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png')
        )
        self.migrante.asistencias.add(AsistenciaDada.objects.get(tipo_asistencia='Primeros Auxilios').pk)
        self.migrante2 = Migrante.objects.create(
            nombre='Roberto',
            paterno='Ramirez',
            materno='Prado',
            pais_origen='AU',
            fecha_nacimiento='1980-08-12',
            fecha_ingreso='1980-08-12',
            sexo='Hombre',
            tipo_identificacion='Licencia',
            identificacion='123456789',
            pais_destino='GB',
            ciudad_destino='Chihuahua',
            motivo=self.mot,
            familiares_destino=True,
            intentos=1,
            familiares_mexico=False,
            lugar_ingreso=self.lug,
            estado='Soltero',
            hijos=1,
            direccion='Palomares 23',
            telefono_contacto='4271098723',
            telefono_recados='4423450911',
            padecimiento=False,
            padecimiento_descripcion='',
            accidentes=True,
            lugar_accidente='Chiapas',
            atracos=False,
            lugar_atraco='',
            denuncio=False,
            con_albergue=self.conoce,
            otros_al=True,
            albergues_descripcion='Guerrero Refugio',
            observaciones='Llego en mal estado fisico',
            imagen_migrante=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png'),
            imagen_firma=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png')

        )
        self.migrante2.asistencias.add(AsistenciaDada.objects.get(tipo_asistencia='Primeros Auxilios').pk)

 # Test connection to migrants registration page logged
 # Parameters: self
 # Return: True

    def test_registrar_url_by_name_log(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        response = c.get(reverse('registrar'))
        self.assertEquals(response.status_code, 200)

 # Test connection to migrants registration page no logged
 # Parameters: self
 # Return: True
    def test_registrar_url_by_name_no_log(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'a', 'password': '1234567890'})
        response = c.get(reverse('registrar'))
        self.assertEquals(response.status_code, 302)

    def test_create_save_migrante(self):
        return self.migrante.save()

    def test_future_born_migrante(self):
        try:
            self.migrante.fecha_nacimiento = datetime.datetime.strptime('2080-10-02', "%Y-%m-%d").date()
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('Fecha futura, imposible que sea de nacimiento', e.message)

    def test_tipo_identificacion_distinta_migrante(self):
        try:
            self.migrante.tipo_identificacion = 'Otra'
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('El tipo de identificacion es invalido', e.message)

    def test_identificacion_distinta_migrante(self):
        try:
            self.migrante.tipo_identificacion = 'Ninguna'
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('Se trato de registrar una identificacion nula, con campos erroneos, ¡rechazado!', e.message)

    def test_duplicar_migrante_identificacion(self):

        try:
            self.migrante.tipo_identificacion = "Pasaporte"
            self.migrante.identificacion = '123456789'
            self.migrante.save()
            self.migrante2.save()
        except Exception as e:
            return self.assertEquals('Identificacion existente', e.message)

    def test_sexo_distinto_migrante(self):
        try:
            self.migrante.sexo = 'Otro'
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('Sexo no registrado', e.message)

# Login test of registered user
# Parameters: self
# Return: True
    def test_login_user(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        return self.assertEquals(response.status_code, 302)

# Login test of unregistered user
# Parameters: self
# Return: True
    def test_login_bad_user(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'a', 'password': '1234567890'})
        return self.assertEquals(response.status_code, 200)
