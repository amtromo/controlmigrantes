// Created by Manuel Garcia
// Description : Graphics js
// Modified by: Manuel Garcia
// Modify date: 30/10/2018

// Load graphics of general
// Parameters: None
// Return: Graphics Loaded
$("#ajaxGeneral").click(function(event) {

  event.preventDefault();

  $('#ajaxGeneral').addClass('active');
  $('#ajaxOD').removeClass('active');
  $('#ajaxOtros').removeClass('active');
  $('#ajaxReg').removeClass('active');

  $("#graphs").html('  <div class="row"><div class="col"><h3 class="sub">Personas en el albergue</h3><div class="chart-container" id="c_1"><canvas id="myChart" width="200px" height="200"></canvas></div></div><div class="col"><h3 class="sub">Estadísticas por sexo</h3><div class="chart-container" id="c_2" ><canvas id="myChart2" width="200px" height="200"></canvas></div></div></div>');
  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Edad_Albergue'
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {
        new Chart(document.getElementById("myChart"), {
          type: 'bar',
          data: {
            labels: data.labels,
            datasets: [data.info, data.info2]
          },
          options: {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });
      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });

  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Sexo_Albergue'
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {
        new Chart(document.getElementById("myChart2"), {
          "type": "doughnut",
          "data": {
            "labels": data.labels,
            "datasets": [data.info]
          }
        });
      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });
});
// Load graphics of others
// Parameters: None
// Return: Graphics Loaded
$("#ajaxOtros").click(function(event) {

  event.preventDefault();
  $('#ajaxGeneral').removeClass('active');
  $('#ajaxOD').removeClass('active');
  $('#ajaxOtros').addClass('active');
  $('#ajaxReg').removeClass('active');
  $("#graphs").html('  <div class="row"><div class="col"><h3 class="sub">Percances de Migrantes</h3><div class="chart-container" id="c_1"><canvas id="myChart" width="200px" height="200"></canvas></div></div><div class="col"><h3 class="sub">Entradas a México</h3><div class="chart-container" id="c_2" ><canvas id="myChart2" width="200px" height="200"></canvas></div></div></div>');
  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Denuncias'
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {
        new Chart(document.getElementById("myChart"), {
          "type": "polarArea",
          "data": {
            "labels": data.labels,
            "datasets": [data.info]
          }
        });
      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });

  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Lugar_Ingreso'
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {

        new Chart(document.getElementById("myChart2"), {
          "type": "line",
          "data": {
            "labels": data.labels,
            "datasets": [data.info]
          },
          "options": {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });

});
// Event Prevent default for multiple ajax requests
// Parameters: None
// Return: Graphics Loaded
function p_ori(elemen) {
  $("#" + elemen.id).unbind("click").bind("click", pais_origen());
}
// Event Prevent default for multiple ajax requests
// Parameters: None
// Return: Graphics Loaded
function p_des(elemen) {
  $("#" + elemen.id).unbind("click").bind("click", pais_destino());
}
// Load graphics of origen
// Parameters: None
// Return: Graphics Loaded
function pais_origen() {
  $("#graphs").html('<h3 class="sub">Estadísticas por país de origen</h3><div class="row"><div class="col-2"><div class="container"><div class="row"><button type="button" class="btn btn-secondary" id="p_origen" name="button" onclick="p_ori(this)">Pais Origen</button></div><div class="row"><button type="button" class="btn btn-primary" id="p_destino"name="button" onclick="p_des(this)">Pais Destino</button></div></div></div><div class="col"><div class="chart-container" id="c_3"><canvas id="myChart3" width="200px" height="200"></canvas></div></div></div>');

  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Pais',
      'op': 'origen'
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {

        new Chart(document.getElementById("myChart3"), {
          "type": "bar",
          "data": {
            "labels": data.labels,
            "datasets": [data.info, data.info2, data.info3, data.info4]
          },
          "options": {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });

}
// Load graphics of destino
// Parameters: None
// Return: Graphics Loaded
function pais_destino() {
  $("#graphs").html('<h3 class="sub">Estadísticas por país de destino</h3><div class="row"><div class="col-2"><div class="container"><div class="row"><button type="button" class="btn btn-primary" id="p_origen" name="button" onclick="p_ori(this)">Pais Origen</button></div><div class="row"><button type="button" class="btn btn-secondary" id="p_destino"name="button" onclick="p_des(this)">Pais Destino</button></div></div></div><div class="col"><div class="chart-container" id="c_3"><canvas id="myChart3" width="200px" height="200"></canvas></div></div></div>');

  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Pais',
      'op': 'destino'
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {

        new Chart(document.getElementById("myChart3"), {
          "type": "bar",
          "data": {
            "labels": data.labels,
            "datasets": [data.info, data.info2, data.info3, data.info4]
          },
          "options": {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });

}

// Load graphics of Origen when first opened
// Parameters: None
// Return: Graphics Loaded

$("#ajaxOD").click(function(event) {
  event.preventDefault();
  $('#ajaxGeneral').removeClass('active');
  $('#ajaxOD').addClass('active');
  $('#ajaxOtros').removeClass('active');
  $('#ajaxReg').removeClass('active');
  pais_origen();
});


// Load graphics of registration
// Parameters: None
// Return: Graphics Loaded
datos = [];
$("#ajaxReg").click(function(event) {
  event.preventDefault();
  $('#ajaxGeneral').removeClass('active');
  $('#ajaxOD').removeClass('active');
  $('#ajaxOtros').removeClass('active');
  $('#ajaxReg').addClass('active');

  $("#graphs").html('<h3 class="sub">Entradas de Migrantes</h3><div class="row"><div class="col-2"><select class="form-control" id="dropdownYear" name=""></select><button type="button" class="btn btn-warning" id="limpiar" name="button" onclick="document.getElementById(' + "'ajaxReg'" + ').click()">Limpiar</button></div><div class="col"><div class="chart-container" id="c_3"><canvas id="myChart3" width="200px" height="200"></canvas></div></div></div>');
  $('#dropdownYear').each(function() {

    var year = (new Date()).getFullYear();
    var current = year;
    limit = year - 1940
    for (var i = 0; i <= limit; i++) {
      if ((year - i) == current)
        $(this).append('<option selected value="' + (year - i) + '">' + (year - i) + '</option>');
      else
        $(this).append('<option value="' + (year - i) + '">' + (year - i) + '</option>');
    }

  });
  $('#dropdownYear').unbind().bind("change", function() {
    var year = $('#dropdownYear').find(":selected").text();
    $.ajax({
      url: '/request_chart/',
      data: {
        'tipo': 'Año',
        'op': year
      },
      dataType: 'json',
      type: 'POST',
      success: function(data) {
        console.log(data)
        if (data.is_valid) {
          datos.push(data.info);
          new Chart(document.getElementById("myChart3"), {
            "type": "line",
            "data": {
              "labels": data.labels,
              "datasets": datos
            },
            "options": {
              scales: {
                yAxes: [{
                  ticks: {
                    beginAtZero: true
                  }
                }]
              }
            }
          });



        } else {
          retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
          $('#retro_ajax').html(retro);
          setTimeout(function() {
            $("#retro_alert_bad").alert('close');
            $(".bad").alert('close');
          }, 5000);
        }
      }
    });
  });

  var year = $('#dropdownYear').find(":selected").text();

  datos = [];
  $.ajax({
    url: '/request_chart/',
    data: {
      'tipo': 'Año',
      'op': year
    },
    dataType: 'json',
    type: 'POST',
    success: function(data) {
      console.log(data)
      if (data.is_valid) {
        datos.push(data.info)
        new Chart(document.getElementById("myChart3"), {
          "type": "line",
          "data": {
            "labels": data.labels,
            "datasets": datos
          },
          "options": {
            scales: {
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
        });

      } else {
        retro = '<div id="retro_alert_bad" class="alert bad alert-danger alert-dismissible fade show" role="alert"><strong>Importante</strong> ' + data.message + ' <br> ' + data.exception + ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
        $('#retro_ajax').html(retro);
        setTimeout(function() {
          $("#retro_alert_bad").alert('close');
          $(".bad").alert('close');
        }, 5000);
      }
    }
  });
});


// Load general graphics
// Parameters: None
// Return: Graphics loaded
$(window).on('load', function() {
  $("#ajaxGeneral").click();
});
