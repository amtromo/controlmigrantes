# Created by framework
# Description : Tests file for proving functionality in the application
# Modified by: Manuel Garcia
# Modify date: 15/10/2018

from django.test import RequestFactory, TestCase
from django.urls import reverse
from .models import Migrante, ConocerAlbergue, AsistenciaDada, LugarIngresoPais, MotivoMigracion
from django.core.files.uploadedfile import SimpleUploadedFile
import datetime
from django.conf import settings
import tempfile
from django.test import Client
from django.contrib.auth.models import User
import json


# Create your tests here.
class MigranteTestCase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='admin', email='admin@es.com', password='1234567890')
        settings.MEDIA_ROOT = tempfile.mkdtemp()
        self.conoce = ConocerAlbergue.objects.create(razon='Internet')
        self.asisten = AsistenciaDada.objects.create(tipo_asistencia='Primeros Auxilios')
        self.lug = LugarIngresoPais.objects.create(lugar='Guatemala')
        self.mot = MotivoMigracion.objects.create(motivo='Violencia')
        self.migrante = Migrante.objects.create(
            nombre='Ricardo',
            paterno='Contreras',
            materno='Quiroz',
            pais_origen='GB',
            fecha_nacimiento='1980-08-12',
            fecha_ingreso='1980-08-12',
            sexo='Hombre',
            tipo_identificacion='Ninguna',
            identificacion='Desconocida',
            pais_destino='AU',
            ciudad_destino='Chihuahua',
            motivo=self.mot,
            familiares_destino=True,
            intentos=1,
            familiares_mexico=False,
            lugar_ingreso=self.lug,
            estado='Casado',
            hijos=1,
            direccion='Palomares 24',
            telefono_contacto='4271098745',
            telefono_recados='4423450918',
            padecimiento=False,
            padecimiento_descripcion='',
            accidentes=True,
            lugar_accidente='Queretaro',
            atracos=False,
            lugar_atraco='',
            denuncio=False,
            con_albergue=self.conoce,
            otros_al=True,
            albergues_descripcion='Chiapas Refugio',
            observaciones='Llego en muy buen estado fisico',
            imagen_migrante=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png'),
            imagen_firma=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png')
        )
        self.migrante.asistencias.add(AsistenciaDada.objects.get(tipo_asistencia='Primeros Auxilios').pk)
        self.migrante2 = Migrante.objects.create(
            nombre='Roberto',
            paterno='Ramirez',
            materno='Prado',
            pais_origen='AU',
            fecha_nacimiento='1980-08-12',
            fecha_ingreso='1980-08-12',
            sexo='Hombre',
            tipo_identificacion='Licencia',
            identificacion='123456789',
            pais_destino='GB',
            ciudad_destino='Chihuahua',
            motivo=self.mot,
            familiares_destino=True,
            intentos=1,
            familiares_mexico=False,
            lugar_ingreso=self.lug,
            estado='Soltero',
            hijos=1,
            direccion='Palomares 23',
            telefono_contacto='4271098723',
            telefono_recados='4423450911',
            padecimiento=False,
            padecimiento_descripcion='',
            accidentes=True,
            lugar_accidente='Chiapas',
            atracos=False,
            lugar_atraco='',
            denuncio=False,
            con_albergue=self.conoce,
            otros_al=True,
            albergues_descripcion='Guerrero Refugio',
            observaciones='Llego en mal estado fisico',
            imagen_migrante=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png'),
            imagen_firma=SimpleUploadedFile(name='Test_Image.png', content=open('media/Image.png', 'rb').read(), content_type='image/png')

        )
        self.migrante2.asistencias.add(AsistenciaDada.objects.get(tipo_asistencia='Primeros Auxilios').pk)

# Test connection to migrants registration page logged
# Parameters: self
# Return: True

    def test_registrar_url_by_name_log(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        response = c.get(reverse('registrar'))
        self.assertEquals(response.status_code, 200)

# Test connection to migrants registration page no logged
# Parameters: self
# Return: True

    def test_registrar_url_by_name_no_log(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'a', 'password': '1234567890'})
        response = c.get(reverse('registrar'))
        self.assertEquals(response.status_code, 302)

    def test_create_save_migrante(self):
        return self.migrante.save()

    def test_future_born_migrante(self):
        try:
            self.migrante.fecha_nacimiento = datetime.datetime.strptime('2080-10-02', "%Y-%m-%d").date()
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('Fecha futura, imposible que sea de nacimiento', e.message)

    def test_tipo_identificacion_distinta_migrante(self):
        try:
            self.migrante.tipo_identificacion = 'Otra'
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('El tipo de identificacion es invalido', e.message)

    def test_identificacion_distinta_migrante(self):
        try:
            self.migrante.tipo_identificacion = 'Ninguna'
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('Se trato de registrar una identificacion nula, con campos erroneos, ¡rechazado!', e.message)

    def test_duplicar_migrante_identificacion(self):

        try:
            self.migrante.tipo_identificacion = "Pasaporte"
            self.migrante.identificacion = '123456789'
            self.migrante.save()
            self.migrante2.save()
        except Exception as e:
            return self.assertEquals('Identificacion existente', e.message)

    def test_sexo_distinto_migrante(self):
        try:
            self.migrante.sexo = 'Otro'
            self.migrante.save()
        except Exception as e:
            return self.assertEquals('Sexo no registrado', e.message)

# Login test of registered user
# Parameters: self
# Return: True
    def test_login_user(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        return self.assertEquals(response.status_code, 302)

# Login test of unregistered user
# Parameters: self
# Return: True
    def test_login_bad_user(self):
        c = Client()
        response = c.post(reverse('login'), {'username': 'a', 'password': '1234567890'})
        return self.assertEquals(response.status_code, 200)

# Test json motivo migracion when logged
# Parameters: self
# Return: True
    def test_json_motivo(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_motivo'), {'motivo': 'Destruccion Masiva'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Motivo agregado')

# Test json motivo migracion when no logged
# Parameters: self
# Return: True
    def test_json_motivo_nolog(self):
        c = Client()
        jsonresponse = c.post(reverse('add_motivo'), {'motivo': 'Destruccion Masiva'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la creacion del motivo o no estas autorizado')

# Test json motivo migracion when motiv exists
# Parameters: self
# Return: True
    def test_json_motivo_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_motivo'), {'motivo': 'Destruccion Masiva'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        jsonresponse = c.post(reverse('add_motivo'), {'motivo': 'Destruccion Masiva'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Motivo existente')

# Test json lugar de ingreso when logged
# Parameters: self
# Return: True
    def test_json_lugar(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_lugar_ingreso'), {'lugar_ingreso': 'Veracruz'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Lugar de ingreso agregado')

# Test json lugar de ingreso when no logged
# Parameters: self
# Return: True
    def test_json_lugar_nolog(self):
        c = Client()
        jsonresponse = c.post(reverse('add_lugar_ingreso'), {'lugar_ingreso': 'Veracruz'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la creacion del lugar de ingreso o no estas autorizado')

# Test json lugar de ingreso when motiv exists
# Parameters: self
# Return: True
    def test_json_lugar_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_lugar_ingreso'), {'lugar_ingreso': 'Veracruz'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        jsonresponse = c.post(reverse('add_lugar_ingreso'), {'lugar_ingreso': 'Veracruz'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Lugar de ingreso existente')

# Test json razon add when logged
# Parameters: self
# Return: True
    def test_json_razon(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_con_albergue'), {'con_albergue': 'Seccion Amarilla'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Razon agregada')

# Test json razon add when no logged
# Parameters: self
# Return: True
    def test_json_razon_nolog(self):
        c = Client()
        jsonresponse = c.post(reverse('add_con_albergue'), {'con_albergue': 'Seccion Amarilla'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la creacion de la razon del conocimiento del albergue o no estas autorizado')

# Test json razon add when motiv exists
# Parameters: self
# Return: True
    def test_json_razon_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_con_albergue'), {'con_albergue': 'Seccion Amarilla'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        jsonresponse = c.post(reverse('add_con_albergue'), {'con_albergue': 'Seccion Amarilla'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Razon existente')

# Test json tipo de asistencia add when logged
# Parameters: self
# Return: True
    def test_json_asistencia(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_asistencias'), {'tipo_asistencia': 'Psicologica'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Tipo de asistencia agregada')

# Test json tipo de asistencia add when no logged
# Parameters: self
# Return: True
    def test_json_asistencia_nolog(self):
        c = Client()
        jsonresponse = c.post(reverse('add_asistencias'), {'tipo_asistencia': 'Psicologica'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la creacion del tipo de asistencia o no estas autorizado')

# Test json tipo asistencia add when motiv exists
# Parameters: self
# Return: True
    def test_json_asistencia_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('add_asistencias'), {'tipo_asistencia': 'Psicologica'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        jsonresponse = c.post(reverse('add_asistencias'), {'tipo_asistencia': 'Psicologica'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Tipo de asistencia existente')

# Test json get Observaciones  when logged
# Parameters: self
# Return: True
    def test_json_get_obs(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('get_observaciones'), {'id': self.migrante.pk}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], '')

# Test json get Observaciones when no logged
# Parameters: self
# Return: True
    def test_json_get_obs_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('get_observaciones'), {'id': self.migrante.pk}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la obtencion de observaciones')

# Test json get Observaciones when migrant do not exists
# Parameters: self
# Return: True
    def test_json_get_obs_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('get_observaciones'), {'id': '299'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'no existe el migrante')

# Test json update Observaciones  when logged
# Parameters: self
# Return: True
    def test_json_update_obs(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('update_obs'), {'id': self.migrante.pk, 'obs': 'Hubo un percanse'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'se han actualizado las observaciones')

# Test json update Observaciones when no logged
# Parameters: self
# Return: True
    def test_json_update_obs_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('update_obs'), {'id': self.migrante.pk, 'obs': 'Hubo un percanse'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la actualización de observaciones')

# Test json update Observaciones when migrant do not exists
# Parameters: self
# Return: True
    def test_json_update_obs_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('update_obs'), {'id': '299', 'obs': 'Hubo un percanse'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'no existe el migrante')

# Test json update Activo  when logged
# Parameters: self
# Return: True
    def test_json_update_act(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('update_act'), {'id': self.migrante.pk}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'se ha actualizado el status')

# Test json update Activo when no logged
# Parameters: self
# Return: True
    def test_json_update_act_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('update_act'), {'id': self.migrante.pk}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la actualización de status')

# Test json update Activo when migrant do not exists
# Parameters: self
# Return: True
    def test_json_update_act_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('update_act'), {'id': '299'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'no existe el migrante')

# Test json update Aceptado  when logged
# Parameters: self
# Return: True
    def test_json_update_acept(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('update_acept'), {'id': self.migrante.pk}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'se ha actualizado el status')

# Test json update Aceptado when no logged
# Parameters: self
# Return: True
    def test_json_update_acept_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('update_acept'), {'id': self.migrante.pk}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la actualización de status')

# Test json update Aceptado when migrant do not exists
# Parameters: self
# Return: True
    def test_json_update_acept_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('update_acept'), {'id': '299'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'no existe el migrante')

# Test json get field  when logged
# Parameters: self
# Return: True
    def test_json_get_field(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('get_field'), {'id': self.migrante.pk, 'field': 'activo'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], '')

# Test json get field when no logged
# Parameters: self
# Return: True
    def test_json_get_field_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('get_field'), {'id': self.migrante.pk, 'field': 'activo'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la peticion del campo')

# Test json get field when migrant do not exists
# Parameters: self
# Return: True
    def test_json_get_field_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('get_field'), {'id': '299', 'field': 'activo'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'no existe el migrante')

# Test json get Queries  when logged
# Parameters: self
# Return: True
    def test_json_get_consult(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('get_consultas'), {}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], '')

# Test json get Queries when no logged
# Parameters: self
# Return: True
    def test_json_get_consult_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('get_consultas'))
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'No estas autenticado o no es un request AJAX')

# Test json get Queries when no AJAX
# Parameters: self
# Return: True
    def test_json_get_consult_noAJAX(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('get_consultas'))
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'No estas autenticado o no es un request AJAX')

# Test json delete migrant when logged
# Parameters: self
# Return: True
    def test_json_delete_migrant(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('delete_migrant'), {'id': self.migrante.pk, 'string': 'borrar migrante ' + str(self.migrante.pk)}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], '')

# Test json delete migrant when no logged
# Parameters: self
# Return: True
    def test_json_delete_migrant_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('delete_migrant'), {'id': self.migrante.pk, 'string': 'borrar migrante ' + str(self.migrante.pk)}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la eliminacion del migrante')

# Test json delete when migrant do not exists
# Parameters: self
# Return: True
    def test_json_delete_migrantd_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('delete_migrant'), {'id': '299', 'string': 'borrar migrante 299'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['exception'], 'no existe el migrante')

# Test json delet migrant bad string
# Parameters: self
# Return: True
    def test_json_delete_migrant_bad_string(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('delete_migrant'), {'id': self.migrante.pk, 'string': ' migrante ' + str(self.migrante.pk)}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'la frase no concordó, ¡no se ha eliminado!')

# Test json get chart  when logged
# Parameters: self
# Return: True
    def test_json_request_chart(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Sexo_Albergue'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], '')

# Test json get chart when no logged
# Parameters: self
# Return: True
    def test_json_request_chart_nolog(self):
        c = Client()
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Sexo_Albergue'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la peticion del chart')

# Test json get chart when not exists
# Parameters: self
# Return: True
    def test_json_request_chart_no_exists(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'No existe'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['message'], 'Error en la peticion del chart')

# Test json get chart  when logged type sexo
# Parameters: self
# Return: True
    def test_json_request_chart_sexo(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Sexo_Albergue'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Personas en el albergue')

# Test json get chart  when logged type edad
# Parameters: self
# Return: True
    def test_json_request_chart_edad(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Edad_Albergue'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Total de Personas') and self.assertEquals(decoded_data['info2']['label'], 'Personas Activas')

# Test json get chart  when logged type denuncias
# Parameters: self
# Return: True
    def test_json_request_chart_denuncias(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Denuncias'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Ocurrencias de Migrantes')

# Test json get chart  when logged type lugar ingreso
# Parameters: self
# Return: True
    def test_json_request_chart_lugar_ingreso(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Lugar_Ingreso'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Lugares de entrada')

# Test json get chart  when logged type pais de origen
# Parameters: self
# Return: True
    def test_json_request_chart_pais_origen(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Pais', 'op': 'origen'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Total por país') and self.assertEquals(decoded_data['info2']['label'], 'Hombres por país') and self.assertEquals(decoded_data['info3']['label'], 'Mujeres por país') and self.assertEquals(decoded_data['info4']['label'], 'Indefinidos por país')

# Test json get chart  when logged type pais de destino
# Parameters: self
# Return: True
    def test_json_request_chart_pais_destino(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Pais', 'op': 'destino'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Total por país') and self.assertEquals(decoded_data['info2']['label'], 'Hombres por país') and self.assertEquals(decoded_data['info3']['label'], 'Mujeres por país') and self.assertEquals(decoded_data['info4']['label'], 'Indefinidos por país')

# Test json get chart  when logged type año
# Parameters: self
# Return: True
    def test_json_request_chart_pais_anio(self):
        c = Client()
        c.post(reverse('login'), {'username': 'admin', 'password': '1234567890'})
        self.migrante.save()
        jsonresponse = c.post(reverse('request_chart'), {'tipo': 'Año', 'op': '2018'}, **{'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'})
        decoded_data = json.loads(jsonresponse.content)
        return self.assertEquals(decoded_data['info']['label'], 'Migrantes por mes en (2018)')
