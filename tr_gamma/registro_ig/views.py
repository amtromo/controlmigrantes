from django.shortcuts import render
from .forms import MigranteForm
from django.http import HttpResponseRedirect
from .models import Migrante, MotivoMigracion, LugarIngresoPais, ConocerAlbergue, AsistenciaDada
from django.http import JsonResponse
from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.core.exceptions import ObjectDoesNotExist
import datetime
from django.utils.timezone import utc
from django.utils import timezone
from .mixins import AjaxFormMixin
from django.http import Http404
from random import randint

# View of Migrants form
# Parameters: requests
# Return: A redirection, a rendered view or an HttpResponse


def StatisticsView(request):
    context = {}
    return render(request, "estadisticas.html", context)


def SearchView(request):
    context = {}
    return render(request, "home.html", context)


def requestQueries(request):

    res = {'exception': ''}
    datas = []
    mig_arr = {}
    if request.is_ajax() and request.user.is_authenticated:
        try:
            todos = Migrante.objects.all()

            for migrante in todos:
                if migrante.eliminado is not True:
                    mig_arr = {}
                    if migrante.activo is True:
                        mig_arr.update(activo='Dentro')
                    else:
                        mig_arr.update(activo='Fuera')
                    mig_arr.update(sistema=migrante.pk)
                    mig_arr.update(nombre=migrante.nombre + ' ' + migrante.paterno + ' ' + migrante.materno)
                    mig_arr.update(fecha_nacimiento=migrante.fecha_nacimiento)
                    mig_arr.update(fecha_ingreso=migrante.fecha_ingreso)
                    mig_arr.update(identificacion=migrante.identificacion)
                    mig_arr.update(origen=migrante.pais_origen.name + ': ' + migrante.lugar_ingreso.lugar)
                    mig_arr.update(destino=migrante.pais_destino.name + ': ' + migrante.ciudad_destino)
                    mig_arr.update(direccion=migrante.direccion)
                    mig_arr.update(contacto=migrante.telefono_contacto + ' ' + migrante.telefono_recados)
                    mig_arr.update(fecha_registro=migrante.fecha_entrada.strftime("%Y-%m-%d"))
                    if migrante.fecha_ultima_salida is not None:
                        mig_arr.update(ultima_salida=migrante.fecha_ultima_salida.strftime("%Y-%m-%d"))
                    else:
                        mig_arr.update(ultima_salida='No')

                    datas.append(mig_arr)

            res.update(data=datas)
        except Exception as e:
            res.update(exception=e.message)
            return JsonResponse(res)
    else:
        res.update(exception='No estas autenticado o no es un request AJAX')

    return JsonResponse(res)

# View of Migrants form
# Parameters: requests
# Return: A redirection, a rendered view or an HttpResponse


def MigranteView(request):

    if request.user.is_authenticated:

        if request.method == 'POST':
            form = MigranteForm(request.POST, request.FILES)

            if form.is_valid():
                print('Forma Valida')
                print(form.clean())
                new_post = form.save()
                print('new_post: ', new_post)
                return HttpResponseRedirect(reverse('migrante', args=(new_post.pk,)))

        else:
            form = MigranteForm()

        context = {'form': form, 'tag': ['Fotos', 'Anexo', 'Albergues:', 'Percanses:', 'Informacion Personal:', 'Migracion:', 'Informacion Basica:']}
        return render(request, "registro_migrante.html", context)

    else:
        return HttpResponseRedirect(reverse('login',))


# Json Response to add new Motivo de Migracion
# Parameters: request
# Return: Json of data

def AddMotivo(request):

    data = {
        'is_valid': False,
        'message': 'Error en la creacion del motivo o no estas autorizado',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_motiv = request.POST.get('motivo')
        if MotivoMigracion.objects.filter(motivo=ajax_motiv):
            data.update(message='Motivo existente')
        else:
            try:
                new_motiv = MotivoMigracion(motivo=ajax_motiv)
                new_motiv.save()
                query = MotivoMigracion.objects.filter(motivo=ajax_motiv)
                if(query is not None):
                    form = MigranteForm()
                    select = str(form['motivo'])
                    id = query.values()
                    data.update(is_valid=True)
                    data.update(message='Motivo agregado')
                    data.update(select=select)
                    data.update(option=id[0]['id'])
            except Exception as e:
                print(e)
                data.update(exception=e.message)
                return JsonResponse(data)

    return JsonResponse(data)


# Json Response to add new Lugar de Ingreso
# Parameters: request
# Return: Json of data

def AddLugarIngreso(request):

    data = {
        'is_valid': False,
        'message': 'Error en la creacion del lugar de ingreso o no estas autorizado',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_lugar_ingreso = request.POST.get('lugar_ingreso')
        if LugarIngresoPais.objects.filter(lugar=ajax_lugar_ingreso):
            data.update(message='Lugar de ingreso existente')
        else:
            try:
                new_lugar = LugarIngresoPais(lugar=ajax_lugar_ingreso)
                new_lugar.save()
                query = LugarIngresoPais.objects.filter(lugar=ajax_lugar_ingreso)
                if(query is not None):
                    form = MigranteForm()
                    select = str(form['lugar_ingreso'])
                    id = query.values()
                    data.update(is_valid=True)
                    data.update(message='Lugar de ingreso agregado')
                    data.update(select=select)
                    data.update(option=id[0]['id'])
            except Exception as e:
                print(e)
                data.update(exception=e.message)
                return JsonResponse(data)

    return JsonResponse(data)


# Json Response to add new Lugar de Ingreso
# Parameters: request
# Return: Json of data

def AddConAlbergue(request):

    data = {
        'is_valid': False,
        'message': 'Error en la creacion de la razon del conocimiento del albergue o no estas autorizado',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_con_albergue = request.POST.get('con_albergue')
        if ConocerAlbergue.objects.filter(razon=ajax_con_albergue):
            data.update(message='Razon existente')
        else:
            try:
                new_razon = ConocerAlbergue(razon=ajax_con_albergue)
                new_razon.save()
                query = ConocerAlbergue.objects.filter(razon=ajax_con_albergue)
                if(query is not None):
                    form = MigranteForm()
                    select = str(form['con_albergue'])
                    id = query.values()
                    data.update(is_valid=True)
                    data.update(message='Razon agregada')
                    data.update(select=select)
                    data.update(option=id[0]['id'])
            except Exception as e:
                print(e.message)
                data.update(exception=e.message)
                return JsonResponse(data)

    return JsonResponse(data)


# Json Response to add new Lugar de Ingreso
# Parameters: request
# Return: Json of data

def AddAsistenciaDada(request):

    data = {
        'is_valid': False,
        'message': 'Error en la creacion del tipo de asistencia o no estas autorizado',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_tipo_asistencia = request.POST.get('tipo_asistencia')
        if AsistenciaDada.objects.filter(tipo_asistencia=ajax_tipo_asistencia):
            data.update(message='Tipo de asistencia existente')
        else:
            try:
                new_tipo = AsistenciaDada(tipo_asistencia=ajax_tipo_asistencia)
                new_tipo.save()
                query = AsistenciaDada.objects.filter(tipo_asistencia=ajax_tipo_asistencia)
                if(query is not None):
                    form = MigranteForm()
                    select = str(form['asistencias'])
                    id = query.values()
                    data.update(is_valid=True)
                    data.update(message='Tipo de asistencia agregada')
                    data.update(select=select)
                    data.update(option=id[0]['id'])
            except Exception as e:
                print(e.message)
                data.update(exception=e.message)
                return JsonResponse(data)

    return JsonResponse(data)


# Json Response to get Observaciones
# Parameters: request
# Return: Json of data

def getObservaciones(request):

    data = {
        'is_valid': False,
        'message': 'Error en la obtencion de observaciones',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        try:
            migrante = Migrante.objects.get(pk=ajax_id)
            data.update(is_valid=True)
            data.update(message='')
            data.update(obs=migrante.observaciones)
        except ObjectDoesNotExist:
            data.update(exception='no existe el migrante')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


# Json Response to delete migrant
# Parameters: request
# Return: Json of data

def deleteMigrant(request):

    data = {
        'is_valid': False,
        'message': 'Error en la eliminacion del migrante',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        ajax_str = request.POST.get('string')

        if ajax_str == 'borrar migrante ' + str(ajax_id):
            try:
                migrante = Migrante.objects.get(pk=ajax_id)
                if not migrante:
                    data.update(exception='no existe el migrante')
                    return JsonResponse(data)
                else:
                    migrante.eliminado = True
                    migrante.save(update_fields=["eliminado"])
                    data.update(is_valid=True)
                    data.update(message='')
            except ObjectDoesNotExist:
                data.update(exception='no existe el migrante')
                return JsonResponse(data)
            except Exception as e:
                data.update(exception=e.message)
                return JsonResponse(data)
        else:
            data.update(message='la frase no concordó, ¡no se ha eliminado!')

    return JsonResponse(data)


# Json Response to update Observaciones
# Parameters: request
# Return: Json of data

def updateObservaciones(request):

    data = {
        'is_valid': False,
        'message': 'Error en la actualización de observaciones',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        ajax_obs = request.POST.get('obs')

        try:
            migrante = Migrante.objects.get(pk=ajax_id)
            if not migrante:
                data.update(exception='no existe el migrante')
                return JsonResponse(data)
            else:
                migrante.observaciones = ajax_obs
                migrante.save(update_fields=["observaciones"])
                data.update(is_valid=True)
                data.update(message='se han actualizado las observaciones')
        except ObjectDoesNotExist:
            data.update(exception='no existe el migrante')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


# Json Response to update imagen
# Parameters: request
# Return: Json of data

def updateImagen(request):

    data = {
        'is_valid': False,
        'message': 'Error en la actualizacion de la imagen',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        ajax_img = request.FILES.get('img')
        ajax_type = request.POST.get('type_str')

        try:
            migrante = Migrante.objects.get(pk=ajax_id)
            if not migrante:
                data.update(exception='no existe el migrante')
                return JsonResponse(data)
            else:
                if(ajax_type == 'firma'):
                    migrante.imagen_firma = ajax_img
                    migrante.save()
                else:
                    migrante.imagen_migrante = ajax_img
                    migrante.save()

                data.update(is_valid=True)
                data.update(message='se ha actualizado la imagen')
        except ObjectDoesNotExist:
            data.update(exception='no existe el migrante')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


# Json Response to update activo
# Parameters: request
# Return: Json of data

def updateActivo(request):

    data = {
        'is_valid': False,
        'message': 'Error en la actualización de status',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        try:
            migrante = Migrante.objects.filter(pk=ajax_id)
            if not migrante:
                data.update(exception='no existe el migrante')
                return JsonResponse(data)
            else:
                status = migrante.first().activo
                migrante.update(activo=(not migrante.first().activo))

                if(status):
                    new_obs = '\n' + str(datetime.datetime.utcnow().replace(tzinfo=utc).strftime("%c")) + ': Se ha cambiado el estado a fuera del albergue del migrante: ' + migrante.first().nombre + ' ' + migrante.first().paterno
                    migrante.update(observaciones=migrante.first().observaciones + new_obs)
                    migrante.update(fecha_ultima_salida=timezone.now())
                else:
                    new_obs = '\n' + str(datetime.datetime.utcnow().replace(tzinfo=utc).strftime("%c")) + ': Se ha cambiado el estado a activo del migrante: ' + migrante.first().nombre + ' ' + migrante.first().paterno
                    migrante.update(observaciones=migrante.first().observaciones + new_obs)

                data.update(is_valid=True)
                data.update(message='se ha actualizado el status')
        except ObjectDoesNotExist:
            data.update(exception='no existe el migrante')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


# Json Response to update aceptado
# Parameters: request
# Return: Json of data

def updateAceptado(request):

    data = {
        'is_valid': False,
        'message': 'Error en la actualización de status',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        try:
            migrante = Migrante.objects.filter(pk=ajax_id)
            if not migrante:
                data.update(exception='no existe el migrante')
                return JsonResponse(data)
            else:
                status = migrante.first().aceptado
                migrante.update(aceptado=(not migrante.first().aceptado))
                if(status):
                    new_obs = '\n' + str(datetime.datetime.utcnow().replace(tzinfo=utc).strftime("%c")) + ': Se ha vetado al migrante: ' + migrante.first().nombre + ' ' + migrante.first().paterno
                    migrante.update(observaciones=migrante.first().observaciones + new_obs)
                else:
                    new_obs = '\n' + str(datetime.datetime.utcnow().replace(tzinfo=utc).strftime("%c")) + ': Se ha vuelto a aceptar al migrante: ' + migrante.first().nombre + ' ' + migrante.first().paterno
                    migrante.update(observaciones=migrante.first().observaciones + new_obs)

                data.update(is_valid=True)
                data.update(message='se ha actualizado el status')
        except ObjectDoesNotExist:
            data.update(exception='no existe el migrante')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


# Json Response to get field of migrant
# Parameters: request
# Return: Json of data

def requestField(request):

    data = {
        'is_valid': False,
        'message': 'Error en la peticion del campo',
        'exception': '', }

    if request.is_ajax() and request.user.is_authenticated:
        ajax_id = request.POST.get('id')
        ajax_id = int(ajax_id)
        ajax_field = request.POST.get('field')
        try:
            migrante = Migrante.objects.filter(pk=ajax_id)
            if not migrante:
                data.update(exception='no existe el migrante')
                return JsonResponse(data)
            else:
                dic = migrante.values(ajax_field)
                dic = list(dic)
                data.update(field=dic[0][ajax_field])
                data.update(is_valid=True)
                data.update(message='')
        except ObjectDoesNotExist:
            data.update(exception='no existe el migrante')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


# Json Response to existence of an id
# Parameters: request
# Return: Json of data

def Id_Check(request):

    is_valid = False
    if request.is_ajax():
        id = request.POST.get('id_identificacion')
        inst_id = request.POST.get('')

        if id == 'Desconocida':
            is_valid = True
        else:
            if inst_id is not None:
                inst_id = int(inst_id)
                if Migrante.objects.filter(identificacion=id).first().id == inst_id:
                    is_valid = True
                else:
                    is_valid = False
            else:
                if Migrante.objects.filter(identificacion=id):
                    is_valid = False
                else:
                    is_valid = True
        data = {
            'is_valid': is_valid
        }
    return JsonResponse(data)


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


# Json Response to get chart
# Parameters: request
# Return: Json of data

def requestChart(request):

    data = {
        'is_valid': False,
        'message': 'Error en la peticion del chart',
        'exception': '',
        'info': {}}

    if request.is_ajax() and request.user.is_authenticated:
        ajax_tipo = request.POST.get('tipo')
        try:
            if ajax_tipo == 'Sexo_Albergue':

                hombres = Migrante.objects.all().filter(sexo='Hombre').count()
                mujeres = Migrante.objects.all().filter(sexo='Mujer').count()
                indef = Migrante.objects.all().filter(sexo='Indefinido').count()
                etiquetas = ['Hombres', 'Mujeres', 'Indefinidos']
                label = 'Personas en el albergue'
                datos = [hombres, mujeres, indef]
                backgroundColor = ['rgb(255, 99, 132)', 'rgb(54, 162, 235)', 'rgb(255, 205, 86)']
                data.update(labels=etiquetas)
                data['info'] = {
                    'label': label,
                    'data': datos,
                    'backgroundColor': backgroundColor}
                data.update(is_valid=True)
                data.update(message='')

            elif ajax_tipo == 'Edad_Albergue':
                now = datetime.datetime.now().date()
                menor_fecha = str(now.year - 18) + '-' + str(now.month) + '-' + str(now.day)
                adulto_fecha = str(now.year - 60) + '-' + str(now.month) + '-' + str(now.day)

                menores = Migrante.objects.all().filter(fecha_nacimiento__gte=menor_fecha).count()
                adultos = Migrante.objects.all().filter(fecha_nacimiento__gte=adulto_fecha).filter(fecha_nacimiento__lte=menor_fecha).count()
                tercera_edad = Migrante.objects.all().filter(fecha_nacimiento__lte=adulto_fecha).count()

                etiquetas = ['Menores', 'Adultos', 'Tercera Edad']
                label = 'Total de Personas'
                backgroundColor = ['rgba(54, 162, 235, 0.2)', 'rgba(255, 206, 86, 0.2)', 'rgba(75, 192, 192, 0.2)']
                borderColor = ['rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)']
                borderWidth = 1
                datos = [menores, adultos, tercera_edad]
                data.update(labels=etiquetas)
                data['info'] = {
                    'label': label,
                    'data': datos,
                    'backgroundColor': backgroundColor,
                    'borderColor': borderColor,
                    'borderWidth': borderWidth,
                }

                menores = Migrante.objects.all().filter(fecha_nacimiento__gte=menor_fecha).filter(activo=True).count()
                adultos = Migrante.objects.all().filter(fecha_nacimiento__gte=adulto_fecha).filter(fecha_nacimiento__lte=menor_fecha).filter(activo=True).count()
                tercera_edad = Migrante.objects.all().filter(fecha_nacimiento__lte=adulto_fecha).filter(activo=True).count()
                label = 'Personas Activas'
                backgroundColor = ['rgba(154, 162, 235, 0.6)', 'rgba(255, 106, 86, 0.2)', 'rgba(75, 192, 92, 0.2)']
                borderColor = ['rgba(154, 162, 235, 60)', 'rgba(155, 106, 86, 1)', 'rgba(75, 192, 92, 1)']
                borderWidth = 1
                datos = [menores, adultos, tercera_edad]
                data.update(info2={})
                data['info2'] = {
                    'label': label,
                    'data': datos,
                    'backgroundColor': backgroundColor,
                    'borderColor': borderColor,
                    'borderWidth': borderWidth,
                }
                data.update(is_valid=True)
                data.update(message='')

            elif ajax_tipo == 'Denuncias':

                si_denuncio = Migrante.objects.all().filter(denuncio=True).count()
                no_denuncio = Migrante.objects.all().filter(denuncio=False).count()
                si_accidentes = Migrante.objects.all().filter(accidentes=True).count()
                no_accidentes = Migrante.objects.all().filter(accidentes=False).count()
                si_atracos = Migrante.objects.all().filter(atracos=True).count()
                no_atracos = Migrante.objects.all().filter(atracos=False).count()

                etiquetas = ['Denuncio', 'Con Accidentes', 'Con Atracos', 'No Denuncio', 'Sin Accidentes', 'Sin Atracos']
                label = 'Ocurrencias de Migrantes'
                datos = [si_denuncio, si_accidentes, si_atracos, no_denuncio, no_accidentes, no_atracos]
                backgroundColor = ['rgb(255, 99, 132)', 'rgb(54, 162, 235)', 'rgb(255, 205, 86)', 'rgba(75, 192, 92, 1)', 'rgba(75, 192, 192, 1)', 'rgb(201, 203, 207)']
                data.update(labels=etiquetas)
                data['info'] = {
                    'label': label,
                    'data': datos,
                    'backgroundColor': backgroundColor}
                data.update(is_valid=True)
                data.update(message='')

            elif ajax_tipo == 'Lugar_Ingreso':

                lugares = LugarIngresoPais.objects.all()
                etiquetas = []
                datos = []
                for place in lugares:
                    etiquetas.append(place.lugar)
                    datos.append(Migrante.objects.all().filter(lugar_ingreso=place).count())

                label = 'Lugares de entrada'
                borderColor = 'rgb(75, 192, 192)'
                fill = False
                lineTension = 0.1
                data.update(labels=etiquetas)
                data['info'] = {
                    'label': label,
                    'data': datos,
                    'fill': fill,
                    'borderColor': borderColor,
                    'lineTension': lineTension}
                data.update(is_valid=True)
                data.update(message='')

            elif ajax_tipo == 'Pais':
                ajax_op = request.POST.get('op')
                etiquetas = []
                datos = []
                datos_hombres = []
                datos_mujeres = []
                datos_indef = []
                if ajax_op == 'origen':
                    all = Migrante.objects.all()
                    for migrante in all:
                        if migrante.pais_origen not in etiquetas:
                            etiquetas.append(migrante.pais_origen)
                    for etiqueta in etiquetas:
                        datos.append(all.filter(pais_origen=etiqueta).count())
                        datos_hombres.append(all.filter(pais_origen=etiqueta).filter(sexo='Hombre').count())
                        datos_mujeres.append(all.filter(pais_origen=etiqueta).filter(sexo='Mujer').count())
                        datos_indef.append(all.filter(pais_origen=etiqueta).filter(sexo='Indefinido').count())

                elif ajax_op == 'destino':
                    all = Migrante.objects.all()
                    for migrante in all:
                        if migrante.pais_destino not in etiquetas:
                            etiquetas.append(migrante.pais_destino)
                    for etiqueta in etiquetas:
                        datos.append(all.filter(pais_destino=etiqueta).count())
                        datos_hombres.append(all.filter(pais_destino=etiqueta).filter(sexo='Hombre').count())
                        datos_mujeres.append(all.filter(pais_destino=etiqueta).filter(sexo='Mujer').count())
                        datos_indef.append(all.filter(pais_destino=etiqueta).filter(sexo='Indefinido').count())

                new_etiquetas = []
                for etiqueta in etiquetas:
                    new_etiquetas.append(etiqueta.name)

                fill = False
                lineTension = 0.1
                data.update(labels=new_etiquetas)
                data['info'] = {
                    'label': 'Total por país',
                    'data': datos,
                    'fill': fill,
                    'backgroundColor': 'rgb(255, 205, 86)',
                    'borderColor': 'rgb(255, 205, 86)',
                    'lineTension': lineTension}
                data.update(info2={})
                data['info2'] = {
                    'label': 'Hombres por país',
                    'data': datos_hombres,
                    'fill': fill,
                    'backgroundColor': 'rgb(54, 162, 235)',
                    'borderColor': 'rgb(54, 162, 235)',
                    'lineTension': lineTension}
                data.update(info3={})
                data['info3'] = {
                    'label': 'Mujeres por país',
                    'data': datos_mujeres,
                    'fill': fill,
                    'backgroundColor': 'rgba(75, 192, 92, 1)',
                    'borderColor': 'rgba(75, 192, 92, 1)',
                    'lineTension': lineTension}
                data.update(info4={})
                data['info4'] = {
                    'label': 'Indefinidos por país',
                    'data': datos_indef,
                    'fill': fill,
                    'backgroundColor': 'rgb(255, 99, 132)',
                    'borderColor': 'rgb(255, 99, 132)',
                    'lineTension': lineTension}
                data.update(is_valid=True)
                data.update(message='')

            elif ajax_tipo == 'Año':
                ajax_op = request.POST.get('op')
                menor_fecha = ajax_op + '-01-01'
                mayor_fecha = ajax_op + '-12-31'

                year = Migrante.objects.all().filter(fecha_entrada__gte=menor_fecha).filter(fecha_entrada__lte=mayor_fecha)

                enero = ajax_op + '-01-01'
                febrero = ajax_op + '-02-01'
                marzo = ajax_op + '-03-01'
                abril = ajax_op + '-04-01'
                mayo = ajax_op + '-05-01'
                junio = ajax_op + '-06-01'
                julio = ajax_op + '-07-01'
                agosto = ajax_op + '-08-01'
                septiembre = ajax_op + '-09-01'
                octubre = ajax_op + '-10-01'
                noviembre = ajax_op + '-11-01'
                diciembre = ajax_op + '-12-01'

                months = [enero, febrero, marzo, abril, mayo, junio, julio, agosto, septiembre, octubre, noviembre, diciembre]
                etiquetas = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
                datos = []
                for month in months:
                    if month != diciembre:
                        datos.append(year.filter(fecha_entrada__gte=month).filter(fecha_entrada__lt=months[months.index(month) + 1]).count())
                    else:
                        datos.append(year.filter(fecha_entrada__gte=month).filter(fecha_entrada__lt=mayor_fecha).count())

                label = 'Migrantes por mes en (' + ajax_op + ')'
                borderColor = 'rgb(' + str(randint(0, 244)) + ', ' + str(randint(0, 244)) + ', ' + str(randint(0, 244)) + ')'
                fill = False
                lineTension = 0.1
                data.update(labels=etiquetas)
                data['info'] = {
                    'label': label,
                    'data': datos,
                    'fill': fill,
                    'borderColor': borderColor,
                    'lineTension': lineTension}
                data.update(is_valid=True)
                data.update(message='')

        except ObjectDoesNotExist:
            data.update(exception='no existen los objetos')
            return JsonResponse(data)
        except Exception as e:
            data.update(exception=e.message)
            return JsonResponse(data)

    return JsonResponse(data)


class MigrantDetailView(DetailView):
    model = Migrante
    template_name = "migrant_detail.html"

    # Render the template if the migrant has not been eliminated
    # Parameters: self, **kwargs
    # Return: Page of the migrants profile
    def render_to_response(self, context, **response_kwargs):
        print(context)
        migrante = context.get('object')
        if migrante.eliminado is True:
            raise Http404
        return self.response_class(request=self.request, template=self.get_template_names(), context=context, **response_kwargs)


class MigrantUpdate(AjaxFormMixin, UpdateView):
    model = Migrante
    form_class = MigranteForm
    template_name = "update_migrant.html"

    # Render the template if the migrant has not been eliminated
    # Parameters: self, context, **response_kwargs
    # Return: Page of the migrants update
    def render_to_response(self, context, **response_kwargs):
        print(context)
        migrante = context.get('object')
        if migrante.eliminado is True:
            raise Http404
        return self.response_class(request=self.request, template=self.get_template_names(), context=context, **response_kwargs)

    # Create response url of success after submiting form
    # Parameters: self, **kwargs
    # Return: Page of the migrants profile

    def get_success_url(self, **kwargs):
        return reverse('migrante', kwargs={'pk': self.object.pk})
