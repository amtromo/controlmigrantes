# Generated by Django 2.1.1 on 2018-10-20 03:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('registro_ig', '0003_auto_20181007_1925'),
    ]

    operations = [
        migrations.AddField(
            model_name='migrante',
            name='aceptado',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='migrante',
            name='activo',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='migrante',
            name='fecha_entrada',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='migrante',
            name='fecha_ultima_salida',
            field=models.DateField(null=True),
        ),
    ]
