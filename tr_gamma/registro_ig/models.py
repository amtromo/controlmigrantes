import datetime
from django.db import models
from django_countries.fields import CountryField
from django.core.validators import MinValueValidator, MaxValueValidator, MaxLengthValidator, MinLengthValidator
from django.core.exceptions import ValidationError
import os
import string
from random import *


def random_string():
    min_char = 16
    max_char = 32
    allchar = string.ascii_letters + string.digits
    place = "".join(choice(allchar) for x in range(randint(min_char, max_char)))
    return place


# Get image path for saving photo of migrant
# Parameters: instance and filename
# Return: String of path to save


def get_image_path(instance, filename):
    return os.path.join('photos', random_string(), filename)

# Get image path for saving firm of migrant
# Parameters: instance and filename
# Return: String of path to save


def get_image_path_firm(instance, filename):
    return os.path.join('firm', random_string(), filename)


class ConocerAlbergue(models.Model):
    razon = models.CharField(max_length=50)

    def __str__(self):
        return self.razon

    class Meta:
        verbose_name = 'Razon del conocimiento del Albergue'
        verbose_name_plural = 'Razones del conocimiento del Albergue'


class AsistenciaDada(models.Model):
    tipo_asistencia = models.CharField(max_length=50)

    def __str__(self):
        return self.tipo_asistencia

    class Meta:
        verbose_name = 'Asistencia Proporcionada'
        verbose_name_plural = 'Asistencias a proporcionar'


class LugarIngresoPais(models.Model):
    lugar = models.CharField(max_length=25)

    def __str__(self):
        return self.lugar

    class Meta:
        verbose_name = 'Lugar de Ingreso al Pais'
        verbose_name_plural = 'Lugares de Ingreso al Pais'


class MotivoMigracion(models.Model):
    motivo = models.CharField(max_length=50)

    def __str__(self):
        return self.motivo

    class Meta:
        verbose_name = 'Motivo de Migracion'
        verbose_name_plural = 'Motivos de Migracion'


class Migrante(models.Model):
    fecha_ingreso = models.DateField(default=datetime.datetime.now)
    nombre = models.CharField(max_length=30, validators=[MinLengthValidator(2), MaxLengthValidator(30)])
    paterno = models.CharField(max_length=30, validators=[MinLengthValidator(1), MaxLengthValidator(30)])
    materno = models.CharField(max_length=30, blank=True, validators=[MaxLengthValidator(30)])
    pais_origen = CountryField()
    fecha_nacimiento = models.DateField()
    indef = 'Indefinido'
    hombre = 'Hombre'
    mujer = 'Mujer'
    sexo_opciones = (
        (indef, 'Indefinido'),
        (hombre, 'Hombre'),
        (mujer, 'Mujer'),
    )

    sexo = models.CharField(
        max_length=10,
        choices=sexo_opciones,
        default=indef,
        validators=[MaxLengthValidator(10)],
    )

    pas = 'Pasaporte'
    visa = 'Visa'
    lic = 'Licencia'
    id = 'Identificacion'
    nin = 'Ninguna'
    opciones_id = (
        (pas, 'Pasaporte'),
        (visa, 'Visa'),
        (lic, 'Licencia'),
        (id, 'Identificacion'),
        (nin, 'Ninguna'),
    )

    tipo_identificacion = models.CharField(
        max_length=14,
        choices=opciones_id,
        default=nin,
        validators=[MaxLengthValidator(14)],
    )
    identificacion = models.CharField(max_length=12, validators=[MaxLengthValidator(12)])
    pais_destino = CountryField()
    ciudad_destino = models.CharField(max_length=25, validators=[MaxLengthValidator(25)])
    motivo = models.ForeignKey(MotivoMigracion, on_delete=models.CASCADE, related_name="motivo_migrante")
    familiares_destino = models.BooleanField(default=False)
    intentos = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    familiares_mexico = models.BooleanField(default=False)
    lugar_ingreso = models.ForeignKey(LugarIngresoPais, on_delete=models.CASCADE, related_name="ingreso_migrante")

    cas = 'Casado'
    sol = 'Soltero'
    viu = 'Viud@'

    opciones_estado = (
        (cas, 'Casado'),
        (sol, 'Soltero'),
        (viu, 'Vidu@')
    )
    estado = models.CharField(
        max_length=7,
        choices=opciones_estado,
        default=sol,
        validators=[MaxLengthValidator(7)],
    )
    hijos = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(16)])

    direccion = models.CharField(max_length=100, blank=True, validators=[MaxLengthValidator(100)])
    telefono_contacto = models.CharField(max_length=15, validators=[MinLengthValidator(10), MaxLengthValidator(15)])
    telefono_recados = models.CharField(max_length=15, validators=[MinLengthValidator(10), MaxLengthValidator(15)])
    padecimiento = models.BooleanField(default=False)
    padecimiento_descripcion = models.TextField(blank=True)
    accidentes = models.BooleanField(default=False)
    lugar_accidente = models.TextField(blank=True)
    atracos = models.BooleanField(default=False)
    lugar_atraco = models.TextField(blank=True)
    denuncio = models.BooleanField(default=False)

    con_albergue = models.ForeignKey(ConocerAlbergue, on_delete=models.CASCADE, related_name='albergue_migrante')
    otros_al = models.BooleanField(default=False)
    albergues_descripcion = models.TextField(blank=True)
    asistencias = models.ManyToManyField(AsistenciaDada)
    observaciones = models.TextField(blank=True)
    imagen_migrante = models.ImageField(upload_to=get_image_path)
    imagen_firma = models.ImageField(upload_to=get_image_path_firm)
    activo = models.BooleanField(default=True)
    aceptado = models.BooleanField(default=True)
    fecha_entrada = models.DateTimeField(null=True, auto_now_add=True)
    fecha_ultima_salida = models.DateField(null=True, blank=True)
    eliminado = models.BooleanField(default=False)

    # foto_migrante = models.ImageField(upload_to="", default="")
    # foto_firma = models.ImageField(upload_to="", default="")
    def __str__(self):
        return self.nombre + ' ' + self.paterno + ' ' + self.materno

    class Meta:
        verbose_name = 'Migrante'
        verbose_name_plural = 'Migrantes'

# Validation of knowing shelter parameters
# Parameters: sender, instance, **kwargs
# Return: Nothing


def validate_conocer_albergue(sender, instance, **kwargs):
    id = instance.razon
    if(id is None):
        raise ValidationError('El campo es nulo')
    if(len(id) <= 1):
        raise ValidationError('La razon no cumple con el minimo de longitud')
    if(len(id) >= 50):
        raise ValidationError('La razon no cumple con el maximo de longitud (50)')

# Validation of place of entrance parameters
# Parameters: sender, instance, **kwargs
# Return: Nothing


def validate_lugar_ingreso(sender, instance, **kwargs):
    id = instance.lugar
    if(id is None):
        raise ValidationError('El campo es nulo')
    if(len(id) <= 1):
        raise ValidationError('La razon no cumple con el minimo de longitud')
    if(len(id) >= 25):
        raise ValidationError('La razon no cumple con el maximo de longitud (25)')

# Validation of motiv parameters
# Parameters: sender, instance, **kwargs
# Return: Nothing


def validate_motivo(sender, instance, **kwargs):
    id = instance.motivo
    if(id is None):
        raise ValidationError('El campo es nulo')
    if(len(id) <= 1):
        raise ValidationError('La razon no cumple con el minimo de longitud')
    if(len(id) >= 50):
        raise ValidationError('La razon no cumple con el maximo de longitud (50)')

# Validation of asistance parameters
# Parameters: sender, instance, **kwargs
# Return: Nothing


def validate_asistencia_dada(sender, instance, **kwargs):
    id = instance.tipo_asistencia
    if(id is None):
        raise ValidationError('El campo es nulo')
    if(len(id) <= 1):
        raise ValidationError('La razon no cumple con el minimo de longitud')
    if(len(id) >= 50):
        raise ValidationError('La razon no cumple con el maximo de longitud (50)')


# Validation of migrants parameters
# Parameters: sender, instance, **kwargs
# Return: Nothing


def validate_migrante(sender, instance, **kwargs):

    id = instance.identificacion
    tipo_id = instance.tipo_identificacion
    fecha_nacimiento = instance.fecha_nacimiento
    fecha_ingreso =  instance.fecha_ingreso
    pk = instance.pk

    if tipo_id == 'Ninguna' and id != 'Desconocida':
        raise ValidationError('Se trato de registrar una identificacion nula, con campos erroneos, ¡rechazado!')

    ahora = datetime.datetime.now()
    fecha_ahora = ahora.strftime("%Y-%m-%d")
    x = isinstance(fecha_nacimiento, str)
    if x is False:
        fecha_nac = fecha_nacimiento.strftime("%Y-%m-%d")
        if fecha_ahora < fecha_nac:
            raise ValidationError('Fecha futura, imposible que sea de nacimiento')
    else:
        if fecha_ahora < fecha_nacimiento:
            raise ValidationError('Fecha futura, imposible que sea de nacimiento')
    
    x = isinstance(fecha_ingreso, str)
    if x is False:
        fecha_ing = fecha_ingreso.strftime("%Y-%m-%d")
        if fecha_ahora < fecha_ing:
            raise ValidationError('Fecha futura, imposible que sea de nacimiento')
    else:
        if fecha_ahora < fecha_ingreso:
            raise ValidationError('Fecha futura, imposible que sea de nacimiento')

    if tipo_id != 'Ninguna' and tipo_id != 'Visa' and tipo_id != 'Pasaporte' and tipo_id != 'Identificacion' and tipo_id != 'Licencia':
        raise ValidationError('El tipo de identificacion es invalido')

    if pk is None:
        if id != 'Desconocida' and tipo_id != 'Ninguna':
            if Migrante.objects.filter(identificacion=id).exists():
                raise ValidationError('Identificacion existente')
    else:
        if id != 'Desconocida' and tipo_id != 'Ninguna':
            if Migrante.objects.filter(identificacion=id).first().id != pk:
                raise ValidationError('Identificacion existente')

    sex = instance.sexo

    if sex != 'Indefinido' and sex != 'Hombre' and sex != 'Mujer':
        raise ValidationError('Sexo no registrado')

    # Validacion del tamaño de las imagenes
    if((instance.imagen_migrante is None) or (instance.imagen_firma is None)):
        if(instance.imagen_migrante is None):
            raise ValidationError('No se cargo imagen de migrante, cargarla de favor')
        if(instance.imagen_firma is None):
            raise ValidationError('No se cargo imagen de firma, cargarla de favor')
    else:
        file_size_mi = instance.imagen_migrante.size
        file_size_fi = instance.imagen_firma.size
        limit_mb = 8

        if file_size_mi > limit_mb * 1024 * 1024:
            raise ValidationError("Max size of file is %s MB" % limit_mb)

        if file_size_fi > limit_mb * 1024 * 1024:
            raise ValidationError("Max size of file is %s MB" % limit_mb)


models.signals.pre_save.connect(validate_migrante, sender=Migrante)
models.signals.pre_save.connect(validate_conocer_albergue, sender=ConocerAlbergue)
models.signals.pre_save.connect(validate_asistencia_dada, sender=AsistenciaDada)
models.signals.pre_save.connect(validate_motivo, sender=MotivoMigracion)
models.signals.pre_save.connect(validate_lugar_ingreso, sender=LugarIngresoPais)
