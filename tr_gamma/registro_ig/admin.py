from django.contrib import admin
from .models import Migrante, ConocerAlbergue, AsistenciaDada, LugarIngresoPais, MotivoMigracion

admin.site.register(Migrante),
admin.site.register(ConocerAlbergue),
admin.site.register(AsistenciaDada),
admin.site.register(LugarIngresoPais),
admin.site.register(MotivoMigracion),
