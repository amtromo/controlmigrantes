"""tr_gamma URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from registro_ig.views import MigranteView, Id_Check, AddMotivo, AddLugarIngreso, AddConAlbergue, AddAsistenciaDada, MigrantDetailView, getObservaciones, updateObservaciones, updateActivo, requestField, updateAceptado, MigrantUpdate, updateImagen, SearchView, requestQueries, deleteMigrant, StatisticsView, requestChart
from django.contrib.auth import views as auth_views
from .settings import BASE_DIR
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth.decorators import login_required
from . import settings
import os
log = os.path.join(BASE_DIR, 'templates/registration/login.html')

urlpatterns = [
    path('', login_required(SearchView), name='home'),
    path('admin/', admin.site.urls),
    path('request_chart/', requestChart, name='request_chart'),
    path('estadisticas/', login_required(StatisticsView), name='estadisticas'),
    path('registro/', MigranteView, name='registrar'),
    path('id_check/', Id_Check, name='id_check'),
    path('delete_migrant/', deleteMigrant, name='delete_migrant'),
    path('update_obs/', updateObservaciones, name='update_obs'),
    path('update_act/', updateActivo, name='update_act'),
    path('update_acept/', updateAceptado, name='update_acept'),
    path('update_img/', updateImagen, name='update_img'),
    path('get_observaciones/', getObservaciones, name='get_observaciones'),
    path('get_field/', requestField, name='get_field'),
    path('get_consultas/', requestQueries, name='get_consultas'),
    path('add_motivo/', AddMotivo, name='add_motivo'),
    path('add_lugar_ingreso/', AddLugarIngreso, name='add_lugar_ingreso'),
    path('add_con_albergue/', AddConAlbergue, name='add_con_albergue'),
    path('add_asistencias/', AddAsistenciaDada, name='add_asistencias'),
    path('accounts/login/', auth_views.LoginView.as_view(template_name=log, redirect_authenticated_user=True)),
    path('migrante/<int:pk>', login_required(MigrantDetailView.as_view()), name="migrante"),
    path('migrante_editar/<int:pk>', login_required(MigrantUpdate.as_view()), name="migrante_editar"),
]

# Agregado para manipular cuentas, login , etc.

urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
